﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using ProjectHunt;

public class PlayerDisplay : MonoBehaviour {

    #region Data
    private Text playerDisplay;
    private Image panel;
    #endregion

    private void OnEnable()
    {
        TurnManager.OnNewTurn += ShowDisplay;
    }

    private void OnDisable()
    {
        TurnManager.OnNewTurn -= ShowDisplay;
    }

    void Awake()
    {
        playerDisplay = GetComponentInChildren<Text>();
        panel = GetComponent<Image>();
        
        playerDisplay.CrossFadeAlpha(0f, 0f, false);
        panel.CrossFadeAlpha(0f, 0f, false);
    }

    private void ShowDisplay()
    {
        playerDisplay.text = TurnManager.Instance.ActiveGameMember.Name;
        StartCoroutine(Show());
    }

    private IEnumerator Show()
    {
        float time = 0;
        playerDisplay.CrossFadeAlpha(1f, 0.1f, false);
        panel.CrossFadeAlpha(1f, 0.1f, false);

        while (time < 1.5f)
        {
            time += Time.deltaTime;
            yield return null;
        }

        playerDisplay.CrossFadeAlpha(0f, 0.5f, false);
        panel.CrossFadeAlpha(0f, 0.5f, false);
    }
}
