﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ProjectHunt;
using System.Linq;

namespace ProjectHunt
{
    public class GroupDisplay : MonoBehaviour {
    
        #region Data
        public static bool dragEnabled = true;

        [SerializeField]
        private GroupDisplayItem groupDisplayItemPrefab;
        [SerializeField]
        private Image newGroupDisplayIcon;
        private List<GroupDisplayItem> displayItemList;
        private UIController uiController;
        [SerializeField]
        private float pulseDuration = 1f;
        private Coroutine pulseRoutine;
        private List<uint> suppressGroupSizeChanges;
        #endregion
    
        #region Events
        private void Awake()
        {
            displayItemList = new List<GroupDisplayItem>();
            suppressGroupSizeChanges = new List<uint>();
        }
    
        private void OnEnable()
        {
            uiController = GetComponentInParent<Transform>().GetComponentInParent<UIController>();
            newGroupDisplayIcon.CrossFadeAlpha(0f, 0f, true);
            TurnManager.OnNewTurn += SortActives;
            EntityDisplayItem.OnEntityDragged += EntityDragged;
            EntityDisplayItem.OnEntityDropped += EntityDropped;
            Group.OnEntitiyCountChange += EntityCountChanged;
        }

        private void OnDisable()
        {
            TurnManager.OnNewTurn -= SortActives;
            EntityDisplayItem.OnEntityDragged -= EntityDragged;
            EntityDisplayItem.OnEntityDropped -= EntityDropped;
            Group.OnEntitiyCountChange -= EntityCountChanged;
        }

        private void Start ()
        {
            SortActives();
            newGroupDisplayIcon.CrossFadeAlpha(0f, 0f, false);
    	}
        #endregion
    
        #region Methods
        public void AddItem(Group group, Color color)
        {
            GroupDisplayItem groupDisplayItem = Instantiate<GroupDisplayItem>(groupDisplayItemPrefab, this.transform);
            displayItemList.Add(groupDisplayItem);
            groupDisplayItem.gameObject.SetActive(true);
            groupDisplayItem.Init(group, color, this);

            //Move new group to the top
            groupDisplayItem.transform.SetAsFirstSibling();
            newGroupDisplayIcon.transform.SetAsFirstSibling();
            
            //Show only groups that are part of the active GameMember
            SortActives();
        }

        public void EntityDragged()
        {
            pulseRoutine = StartCoroutine(NewGroupImagePulse());
        }

        public void EntityDropped(GroupDisplayItem fromDisplay, EntityDisplayItem entityDisplayItem, Vector2 position)
        {
            if (!dragEnabled)
                return;
            StopCoroutine(pulseRoutine);
            newGroupDisplayIcon.CrossFadeAlpha(0f, 0f, false);
            if (RectTransformUtility.RectangleContainsScreenPoint(newGroupDisplayIcon.transform as RectTransform, position))
            {
                GameMember currentGameMember = TurnManager.Instance.GetGameMemberByID(fromDisplay.MemberID);
                //Suppressing Group Size event changes (performance optimization so we don't need to Destroy the Display Item)
                suppressGroupSizeChanges.Add(fromDisplay.GroupID);
                if(currentGameMember.GetGroupByID(fromDisplay.GroupID).Size > 1 && currentGameMember.SplitGroup(fromDisplay.GroupID, new Entity[1] { entityDisplayItem.Entity }))
                { //Group Split successful
                    fromDisplay.RemoveEntitiyDisplayItem(entityDisplayItem);
                    Destroy(entityDisplayItem.gameObject);
                }
            }
            else
            {
                foreach (var toDisplay in displayItemList)
                {
                    if (toDisplay.gameObject.activeSelf && !(fromDisplay == toDisplay) &&
                        RectTransformUtility.RectangleContainsScreenPoint(toDisplay.transform as RectTransform, position))
                    {
                        GameMember currentGameMember = TurnManager.Instance.GetGameMemberByID(fromDisplay.MemberID);
                        if(currentGameMember.GetGroupByID(fromDisplay.GroupID).Position == currentGameMember.GetGroupByID(toDisplay.GroupID).Position)
                        {
                            //Suppressing Group Size event changes (performance optimization so we don't need to Destroy the Display Item)
                            suppressGroupSizeChanges.Add(fromDisplay.GroupID);
                            suppressGroupSizeChanges.Add(toDisplay.GroupID);
                            if (currentGameMember.MoveEntityFromTo(fromDisplay.GroupID, toDisplay.GroupID, new Entity[1] { entityDisplayItem.Entity }))
                            { //Reassigning Entity to different Group successful
                                fromDisplay.RemoveEntitiyDisplayItem(entityDisplayItem);
                                toDisplay.AddEntityDisplayItem(entityDisplayItem);
                            }
                        }
                        else
                        {
                            UITextController.Instance.QueueTextBox(textBoxPosition.topCenter, null, "Gruppen müssen auf dem selben Feld stehen um sie zusammen zu führen!", 2f);
                        }
                        break;
                    }
                }
            }
            fromDisplay.UpdateSlideWithDelay();
        }

        /// <summary>
        /// OPTIMIERUNGSBEDARF!!! SEHR UNSCHÖN!!! OBJEKTE WERDEN ZERSTÖRT, DA GRUPPEN EINZELN BEHANDELT WERDEN!!!
        /// </summary>
        /// <param name="group"></param>
        public void EntityCountChanged(Group group)
        {
            if(suppressGroupSizeChanges.Remove(group.GroupID))
                return;

            var displayItem = displayItemList.SingleOrDefault(p => p.GroupID == group.GroupID && p.MemberID == group.MemberID);
            if(group.Size > displayItem.Size)
            { //Gruppe ist größer
                var entities = group.GetMembers();
                foreach (var entity in entities)
                {
                    if (!displayItem.ContainsEntity(entity))
                        displayItem.CreateNewEntityDisplayItem(entity);
                }
            }
            else if (group.Size < displayItem.Size)
            {// Grupper ist kleiner
                var entities = group.GetMembers();
                var displayEntities = displayItem.GetEntityDisplayItemList();
                for (int i = 0; i < displayEntities.Count; i++)
                {
                    if (!entities.Contains(displayEntities[i].Entity))
                    {
                        var item = displayItem.GetEntityDisplayItem(displayEntities[i].Entity);
                        displayItem.RemoveEntitiyDisplayItem(item);
                        Destroy(item.gameObject);
                        break;
                    }
                }
            }
            else
            {
                Debug.Assert(true, "Group Size did not change!!");
            }
        }

        private IEnumerator NewGroupImagePulse()
        {
            while (true)
            {
                newGroupDisplayIcon.CrossFadeAlpha(0.75f, pulseDuration, false);
                yield return new WaitForSeconds(pulseDuration);
                newGroupDisplayIcon.CrossFadeAlpha(0.1f, pulseDuration, false);
                yield return new WaitForSeconds(pulseDuration);
            }
        } 

        public void SetHighlight(uint groupId)
        {
            foreach (var item in displayItemList)
            {
                if (item.gameObject.activeSelf)
                {
                    if(item.GroupID == groupId)
                    {
                        item.SetHighlight(true);
                    }
                    else
                    {
                        item.SetHighlight(false);
                    }
                }
            }
        }

        public bool RemoveItemByID(uint groupID, uint memberID)
        {
            var item = displayItemList.Single(c => c.GroupID == groupID && c.MemberID == memberID);
            if(item != null)
            {
                displayItemList.Remove(item);
                Destroy(item.gameObject);
                return true;
            }
            return false;
        }
    
        private void SortActives()
        {
            GameMember activeGameMember = TurnManager.Instance.ActiveGameMember;

            foreach (var item in displayItemList)
            {
                if(activeGameMember.MemberID == item.MemberID)
                {
                    item.gameObject.SetActive(true);
                }
                else
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
    
        public void ItemClicked(uint groupId, uint memberID)
        {
            uiController.GroupClicked(groupId, memberID);
        }
        #endregion
    }
}
