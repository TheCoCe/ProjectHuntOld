﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Playables;

public class SubtitleController : MonoBehaviour {

    #region ~~SINGLETON~~
    private static SubtitleController instance;

    public static SubtitleController Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<SubtitleController>();
            return instance;
        }

    }
    #endregion

    [SerializeField]
    private Text textArea;

    [SerializeField]
    private Text charText;

    [SerializeField]
    private Text continueText;

    private bool isDone;

    public bool IsDone
    {
        get
        {
            return isDone;
        }
    }

    private List<Subtitle> subtitles = new List<Subtitle>();

    private string subtitleToShow;

    private void OnEnable()
    {
        if (subtitles.Count >= 1)
        {
            subtitleToShow = subtitles[0].Text;
            charText.text = subtitles[0].Character;
            subtitles.Remove(subtitles[0]);
            StartCoroutine(ShowSubtitle(subtitleToShow));
            return;
        }
        Debug.Log("End of Subtitle");
    }


    public IEnumerator ShowSubtitle(string text)
    {
        continueText.gameObject.SetActive(false);
        textArea.text = "";
        foreach (char item in text.ToCharArray())
        {
            textArea.text += item;
            yield return new WaitForSeconds(0.01f);
        }
    }

    public void SetSubtitles(List<Subtitle> subtitles)
    {
        this.subtitles.AddRange(subtitles);
    }

    public void AddSubtitle(Subtitle subtitle)
    {
        this.subtitles.Add(subtitle);
    }

    public void RemoveSubtitle(Subtitle subtitle)
    {
        if (subtitles.Contains(subtitle))
            subtitles.Remove(subtitle);
        else
            Debug.LogError("subtitle not in List");
    }

    public void OnDisable()
    {
        //StorySceneController.Instance.Pause();
        //continueText.gameObject.SetActive(true);
    }
}
