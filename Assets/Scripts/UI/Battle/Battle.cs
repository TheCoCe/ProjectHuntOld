﻿using UnityEngine;
using UnityEngine.UI;
using ProjectHunt;
using System.Collections.Generic;
using System.Collections;

public class Battle : MonoBehaviour {

    #region Data
    public RectTransform battleGroup;
    public Slider playerGroupSlider;
    public Slider aiGroupSlider;

    private int playerGroupSize;
    private IReadOnlyCollection<Entity> aiGroupMembers, playerGroupMembers;
    private int playerGroupHPMax, playerGroupHPCurrent;
    private int aiGroupHPMax, aiGroupHPCurrent;
    private int aiDamagePerHit, aiDamageOffset;
    private float secondsBetweenAiDamage, currentTime;

    bool battle = false;
    #endregion


    #region Properties
    public static Battle Instance
    {
        get; private set;
    }
    #endregion

    #region Events
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    private void Update ()
    {
        if (battle)
        {
            if (Input.GetMouseButtonDown(0))
            {
                DealDamageToAi(9 * playerGroupSize);
            }
            currentTime += Time.deltaTime;
            if(currentTime > secondsBetweenAiDamage)
            {
                currentTime = 0f;
                DealDamageToPlayer(Random.Range(0, aiDamagePerHit));
            }
        }
	}
    #endregion

    #region Methods
    public void SetupBattle(Group playerGroup, AIGroup aiGroup, int aiDamagePerHit = 50, float secondsBetweenAiDamage = 0.5f)
    {
        playerGroupSize = playerGroup.Size;
        playerGroupHPMax = 0;
        this.aiDamagePerHit = aiDamagePerHit;
        this.secondsBetweenAiDamage = secondsBetweenAiDamage;

        playerGroupMembers = playerGroup.GetMembers();
        foreach (var entity in playerGroupMembers)
        {
            playerGroupHPMax += entity.MaxHP;
            playerGroupHPCurrent += entity.HP;
        }

        aiGroupMembers = aiGroup.GetMembers();
        foreach (var entity in aiGroupMembers)
        {
            aiGroupHPMax += entity.MaxHP;
            aiGroupHPCurrent += entity.HP;
        }

        playerGroupSlider.value = (float)playerGroupHPCurrent / (float)playerGroupHPMax;
        aiGroupSlider.value = (float)aiGroupHPCurrent / (float)aiGroupHPMax;

        battleGroup.gameObject.SetActive(true);
        battle = true;
    }

    //TODO: Besser machen
    public void DealDamageToAi(int dmg)
    {
        aiGroupHPCurrent -= dmg;
        aiGroupSlider.value = (float)aiGroupHPCurrent / (float)aiGroupHPMax;
        if(aiGroupHPCurrent <= 0)
        {
            UITextController.Instance.QueueTextBox(textBoxPosition.middleCenter, null,
                "Ihr habt das Monster besiegt. Es wird niemanden mehr bedrohen können.", 0);
            battle = false;
            StartCoroutine(GameOver());
        }
    }

    //TODO: Besser machen
    public void DealDamageToPlayer(int dmg)
    {
        playerGroupHPCurrent -= dmg;
        playerGroupSlider.value = (float)playerGroupHPCurrent / (float)playerGroupHPMax;
        if (playerGroupHPCurrent <= 0)
        {
            UITextController.Instance.QueueTextBox(textBoxPosition.middleCenter, null, 
                "Das Monster hat euch bezwungen. Ihr seid gerade so mit dem Leben davon gekommen. Es ist zu gefährlich es weiter zu verfolgen. GAME OVER!", 0);
            battle = false;
            StartCoroutine(GameOver());
        }
    }

    /// <summary>
    /// TODO: MAKE NICE. This is to go back to main menu if the player looses
    /// </summary>
    /// <returns></returns>
    public IEnumerator GameOver()
    {
        yield return new WaitForSeconds(10f);
        UIController.Instance.MainMenuButton();
        battleGroup.gameObject.SetActive(false);
    }

    public void EndBattle()
    {
        foreach (var entity in playerGroupMembers)
            entity.HP = playerGroupHPCurrent / playerGroupMembers.Count;
        foreach (var entity in aiGroupMembers)
            entity.HP = aiGroupHPCurrent / aiGroupMembers.Count;

        battleGroup.gameObject.SetActive(false);
        battle = false;
    }
    #endregion
}
