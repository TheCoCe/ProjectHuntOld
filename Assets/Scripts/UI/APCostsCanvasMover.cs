﻿using ProjectHunt;
using UnityEngine;
using UnityEngine.UI;

public class APCostsCanvasMover : MonoBehaviour {

    #region Data
    public float yOffset = 2f;
    private Transform cameratransform;
    private RectTransform recttransform;
    private Quaternion originalrotation;
    private CanvasGroup canvasGroup;
    private Text text;
    private float lastTime;
    private Group activeGroup;
    #endregion

    #region Events
    private void Awake()
    {
        
    }

    private void Start()
    {
        cameratransform = Camera.main.transform;
        originalrotation = transform.rotation;
        recttransform = GetComponent<RectTransform>();
        canvasGroup = GetComponentInChildren<CanvasGroup>();
        text = GetComponentInChildren<Text>();
    }

    private void OnEnable()
    {
        InputManager.OnTileHover += SetPositionAndText;
    }

    private void OnDisable()
    {
        InputManager.OnTileHover -= SetPositionAndText;   
    }

    private void Update()
    {
        if (Time.realtimeSinceStartup > lastTime + 0.5f)
        {
            if (canvasGroup.alpha < 1f)
            {
                canvasGroup.alpha += .1f;
            }
            ScaletoCam();
            Billboard();
        }
        else
        {
            canvasGroup.alpha = 0f;
        }
    }
    #endregion

    #region Methods
    private void SetPositionAndText(GridTile gridtile)
    {
        Vector3 coord = new Vector3(
                gridtile.transform.position.x,
                gridtile.transform.position.y + yOffset,
                gridtile.transform.position.z);
        transform.position = coord;

        if(text != null)
        {
            text.text = (PrefabMap.Instance.GetMovementCostByType(gridtile.biomeType) +
                activeGroup.MovementCoefficientArray[(int)gridtile.biomeType])
                .ToString();

            lastTime = Time.realtimeSinceStartup;
        }
    }

    private void Billboard()
    {
       transform.rotation = cameratransform.rotation * originalrotation;
    }

    private void ScaletoCam()
    {
        float dist = Vector3.Distance(transform.position, cameratransform.position);
        dist /= 1000;
        recttransform.localScale = new Vector2(dist, dist);
    }

    public void SetActiveGroup(Group group)
    {
        activeGroup = group;
    }
    #endregion
}
