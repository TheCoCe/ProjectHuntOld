﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System;

namespace ProjectHunt
{
    public class UIController : MonoBehaviour
    {
        #region Data
        //UI
        [SerializeField]
        private APCostsCanvasMover apCostCanvas;
        private EndTurnButton endTurnButton;
        private AbilityBarMover abilityBar;
        [SerializeField]
        private ApBar apBar;
        [SerializeField]
        private GroupDisplay groupDisplay;

        //Group Icons
        public Color[] groupIconColors;
        private GroupIconPin activeGroupIcon;
        private List<GroupIconPin> groupIcons;
        private GameObject groupIconCanvas;

        //Other
        [SerializeField]
        private CameraController cameraController;
        [SerializeField]
        private MapData mapData;
        private InputManager raycaster;

        //Turn handling
        private TurnManager turnManager;
        private GameMember activeGameMember;
        private Group activeGroup;

        //Hover Movement Path
        private List<GridTile> movementPath;

        #endregion

        #region Properties
        public static UIController Instance
        {
            get;
            private set;
        }

        public InputManager Raycaster
        {
            get
            {
                return raycaster;
            }
        }

        public void StopIconMovement()
        {
            (activeGroupIcon as PlayerGroupIconPin).StopMovement();
        }

        public Group ActiveGroup
        {
            get { return activeGroup; }
            private set
            {
                activeGroup = value;
                Debug.Log("activeGroup switched to: " + activeGroup.GroupID);
                bool foundGroupIcon = SetActiveGroupIconById(ActiveGroup.GroupID, ActiveGroup.MemberID);
                Debug.Assert(foundGroupIcon, "No GroupIcon found for current active group!");
                apBar.SetActiveGroup(activeGroup);
                //apCostCanvas.SetActiveGroup(activeGroup);
                groupDisplay.SetHighlight(activeGroup.GroupID);

                CenterCamera();
                if (activeGroup is AIGroup)
                    ClearMovementPathDisplay();
                else
                    DisplayMovementPath(null, true);
            }
        }

        public GroupIconPin ActiveGroupIcon
        {
            get
            {
                return activeGroupIcon;
            }
        }
        #endregion
    
        #region Events
        private void Awake()
        {
            Instance = this;
            turnManager = TurnManager.Instance;
            groupIcons = new List<GroupIconPin>();
            movementPath = new List<GridTile>();
            groupIconCanvas = new GameObject("GroupIconCanvas");
            groupIconCanvas.AddComponent<Canvas>();
            groupIconCanvas.AddComponent<CanvasScaler>().referencePixelsPerUnit = 1f;
            groupIconCanvas.transform.parent = this.transform;

            raycaster = GetComponent<InputManager>();
            Debug.Log("UIController Awake");
        }
    
        void Start()
        {
            //apCostCanvas = Instantiate<GameObject>(apCostCanvas.gameObject, this.transform).GetComponent<APCostsCanvasMover>();

            activeGameMember = turnManager.ActiveGameMember;
            ActiveGroup = activeGameMember.GetFirstGroup();
    
            foreach (GroupIconPin icon in groupIcons)
            { 
                if (activeGameMember.MemberID == icon.MemberID)
                    (icon as PlayerGroupIconPin)?.SetInteractable(true);
                else
                    (icon as PlayerGroupIconPin)?.SetInteractable(false);
            }

            endTurnButton = GetComponentInChildren<EndTurnButton>();        
            abilityBar = GetComponentInChildren<AbilityBarMover>();

            Debug.Log("UIController Start");
        }
    
        private void OnEnable()
        {
            InputManager.OnTileClicked += Clicked;
            InputManager.OnTileHover += Hover;
            GameMember.OnGroupCreated += CreateGroupIcon;
            AIGameMember.OnAIGroupCreated += CreateGroupIcon;
            GameMember.OnGroupRemoved += RemoveGroupIcon;
            TurnManager.OnNewTurn += NewTurn;
            PlayerGroupIconPin.OnPlayerIconStoppedMoving += ClearMovementPathDisplay;
            PlayerGroupIconPin.OnPlayerIconStoppedMoving += CheckAPLeft;
        }
    
        private void OnDisable()
        {
            InputManager.OnTileClicked -= Clicked;
            InputManager.OnTileHover -= Hover;
            GameMember.OnGroupCreated -= CreateGroupIcon;
            AIGameMember.OnAIGroupCreated -= CreateGroupIcon;
            GameMember.OnGroupRemoved -= RemoveGroupIcon;
            TurnManager.OnNewTurn -= NewTurn;
            PlayerGroupIconPin.OnPlayerIconStoppedMoving -= ClearMovementPathDisplay;
            PlayerGroupIconPin.OnPlayerIconStoppedMoving -= CheckAPLeft;
        }

        private void Update()
        {
            
        }
        #endregion

        #region Methods
        private void Hover(GridTile tile)
        {
            if(activeGroupIcon.DestinationReached)
            {
#if UNITY_DEBUG
                if (Input.GetKey(KeyCode.Space))
#endif
                {
                    if (Input.GetKey(KeyCode.LeftShift))
                    {
                        PathRequestManager.RequestPath(activeGroup.Position, tile.Coord, DisplayMovementPath, AStarMode.ShortestPath);
                    }
                    else
                    {
                        PathRequestManager.RequestPath(activeGroup.Position, tile.Coord, DisplayMovementPath, AStarMode.ShortestPathWithRoads);
                    }
                }
            }
        }

        private void Clicked(GridTile tile)
        {
            if(activeGroupIcon.DestinationReached && movementPath.Count > 1)
            {
                if(mapData.IsValidTile(tile.Coord, false))
                {
                    movementPath[0].gameObject.layer = 0;
                    activeGroupIcon.StartMovement(new Queue<GridTile>(movementPath.GetRange(1, movementPath.Count - 1)));
                }
                else
                {
                    UITextController.Instance.QueueTextBox(textBoxPosition.middleCenter, null, "You can't move to this tile.", 2f);
                }
            }
        }

        /// <summary>
        /// Highlights every tile in the current path
        /// </summary>
        /// <param name="path"></param>
        /// <param name="success"></param>
        private void DisplayMovementPath(List<GridTile> path, bool success)
        {
            if (success && activeGroupIcon.DestinationReached)
            {
                ClearMovementPathDisplay();
                GridTile temp;
                mapData.GetTileAtCoord(activeGroup.Position, out temp);
                movementPath.Add(temp);
                if (path != null)
                    movementPath.AddRange(path);
                if (movementPath != null)
                {
                    for (int i = 0; i < movementPath.Count; i++)
                    {
                        movementPath[i].gameObject.layer = 8;
                    }
                }
            }
        }

        /// <summary>
        /// Clears all highlighted tiles from the last displayed path
        /// </summary>
        private void ClearMovementPathDisplay()
        {
            if (movementPath != null)
            {
                for (int i = 0; i < movementPath.Count; i++)
                {
                    movementPath[i].gameObject.layer = 0;
                }
                movementPath.Clear();
            }
            else
            {
                movementPath = new List<GridTile>();
            }
        }

        /// <summary>
        /// Method that is called when a Group was clicked
        /// </summary>
        /// <param name="clickedGroupID">ID of the clocked group</param>
        public void GroupClicked(uint clickedGroupID, uint clickedMemberID)
        {
            if (clickedMemberID == activeGameMember.MemberID && clickedGroupID != ActiveGroup.GroupID)
            {
                if (activeGameMember.HasGroup(clickedGroupID))
                {
                    ActiveGroup = activeGameMember.GetGroupByID(clickedGroupID);
                }
            }
        }
    
        /// <summary>
        /// Creates a group icon (Is automatically called when a group gets created)
        /// </summary>
        /// <param name="group">Group that the icon represents</param>
        private void CreateGroupIcon(Group group)
        {
            GroupIconPin icon;
            if (group is AIGroup)
            {
                icon = Instantiate(PrefabMap.Instance.monsterIcon, groupIconCanvas.transform).GetComponentInChildren<GroupIconPin>();
            }
            else
            {
                icon = Instantiate(PrefabMap.Instance.groupIcon, groupIconCanvas.transform).GetComponentInChildren<GroupIconPin>();
                //Color color = groupIconColors.Length >= 1 && group.GroupID <= groupIconColors.Length - 1 ? groupIconColors[group.GroupID] : Color.white;
                icon.SetColor(groupIconColors[(int)group.GroupID % (groupIconColors.Length - 1)]);
                groupDisplay.AddItem(group, groupIconColors[(int)group.GroupID % (groupIconColors.Length - 1)]);
            }
            icon.Init(group, this);
            Debug.Log("ICON GROUP ID: " + group.GroupID);
            groupIcons.Add(icon);
            //TODO: Remove Debug
            //icon.SetEntityCount(countTest++);
        }

        /// <summary>
        /// Removed all UI Elements corresponding to the group
        /// </summary>
        /// <param name="group">Group whose UI needs to be removed</param>
        private void RemoveGroupIcon(Group group, Group nextActiveGroup)
        {
            //Remove Group icon pin
            foreach (GroupIconPin item in groupIcons)
            {
                if (item.MemberID == group.MemberID && item.GroupID == group.GroupID)
                {
                    groupIcons.Remove(item);
                    Destroy(item.gameObject);
                    break;
                }
            }
            //Remove Group Display Item
            groupDisplay.RemoveItemByID(group.GroupID, group.MemberID);
            //Reset active group
            ActiveGroup = nextActiveGroup;
        }

        /// <summary>
        /// Set's the active group by ID if the ID exists
        /// </summary>
        /// <param name="groupID">Group ID</param>
        /// <param name="memberID">Member ID</param>
        /// <returns>Returns true if the setting the active group was successful</returns>
        private bool SetActiveGroupIconById(uint groupID, uint memberID)
        {
            for (int i = 0; i < groupIcons.Count; i++)
            {
                if (memberID == groupIcons[i].MemberID && groupID == groupIcons[i].GroupID)
                {
                    (activeGroupIcon as PlayerGroupIconPin)?.SetHighlight(false);
                    activeGroupIcon = groupIcons[i];
                    (activeGroupIcon as PlayerGroupIconPin)?.SetHighlight(true);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Queueing AI movement and starting Coroutine to move AI if not already running
        /// </summary>
        /// <param name="group"></param>
        /// <param name="path"></param>
        public void StartAIMovement(AIGroup group, Queue<GridTile> path)
        {
            GroupIconPin c = groupIcons.SingleOrDefault(p => p.MemberID == group.MemberID && p.GroupID == group.GroupID);
            c.StartMovement(path);
        }

        /// <summary>
        ///  Gets the new activeMember and sets the active group
        /// </summary>
        private void NewTurn()
        {
            activeGameMember = turnManager.ActiveGameMember;
            ActiveGroup = activeGameMember.GetFirstGroup();

            if (activeGameMember is AIGameMember)
            {
                foreach (PlayerGroupIconPin icon in groupIcons.OfType<PlayerGroupIconPin>())
                {
                    //(icon as PlayerGroupIconPin)?.SetInteractable(false);
                    icon.SetInteractable(false);
                }
                //TODO: schlauer machen
                DisablePlayerUI();
            }
            else
            {
                foreach (PlayerGroupIconPin icon in groupIcons.OfType<PlayerGroupIconPin>())
                {
                    if (icon.MemberID == activeGameMember.MemberID)
                    {
                        //(icon as PlayerGroupIconPin)?.SetInteractable(true);
                        icon.SetInteractable(true);
                    }
                    else
                    {
                        //(icon as PlayerGroupIconPin)?.SetInteractable(false);
                        icon.SetInteractable(false);
                    }
                }
                EnablePlayerUI();
            }
        }

        public void DisablePlayerUI()
        {
            raycaster.enabled = false;
            endTurnButton.SetInteractable(false);
            abilityBar.SetInteractable(false);
        }

        public void EnablePlayerUI()
        {
            raycaster.enabled = true;
            endTurnButton.SetInteractable(true);
            abilityBar.SetInteractable(true);
        }

        public void CheckAPLeft()
        {
            bool canMove = false;
            //List<GridTile> tiles;
            GridTile[] tilesArray;
            //tiles = mapData.GetTilesInRadius(activeGroup.Position, 1);
            mapData.GetNeighbours(activeGroup.Position, out tilesArray, false);
            for (int i = 0; i < tilesArray.Length; i++)
            {
                if (activeGroup.CurrentAP >= tilesArray[i]?.MovementCost)
                    return;
            }

            int groupId = -1;
            Dictionary<uint, Group> groups = activeGameMember.GetGroups();
            foreach (var group in groups.Values)
            {
                if (group.GroupID == activeGroup.GroupID)
                    continue;
                //tiles = mapData.GetTilesInRadius(group.Position, 1, false);
                mapData.GetNeighbours(activeGroup.Position, out tilesArray, false);
                for (int i = 0; i < tilesArray.Length; i++)
                {
                    if(group.CurrentAP >= tilesArray[i]?.MovementCost)
                    {
                        canMove = true;
                        groupId = (int)group.GroupID;
                        break;
                    }
                }
            }
            if (!canMove)
            {
                endTurnButton.Pulse(1f);
            }
            else if(groupId != -1)
            {
                //TODO: highlight group with AP left
            }
        }

        #region ButtonMethods

        /// <summary>
        /// Messages the Turn Manager that the current turn is over.
        /// </summary>
        public void EndTurn()
        {
            turnManager.EndTurn();
            endTurnButton.StopPulse();
        }
        
        public void CenterCamera()
        {
            if (cameraController == null)
                Debug.Log("NULL");
            cameraController.CenterCamera((GridCoord)activeGroup.Position);
        }
        
        /// <summary>
        /// Opens the Main Menu Scene.
        /// </summary>
        public void MainMenuButton()
        {
            SceneManager.LoadScene("Menu");
        }

        public void AddGroup()
        {
            activeGameMember.CreateGroup(new GridCubeCoord(0, 0, 0), null);
        }

        #endregion

#endregion
    }
}