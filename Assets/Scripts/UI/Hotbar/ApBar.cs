﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectHunt
{
    public class ApBar : MonoBehaviour {
        #region Data
        [SerializeField]
        private RectTransform panel;
        [SerializeField]
        private RectTransform apBorder;
        [SerializeField]
        private RectTransform apBorderEnd;
        private RectTransform[] apArray;
        [SerializeField]
        private int apCount = 0;
        private RectTransform[] apBorders;
        private Group activeGroup;
        [SerializeField]
        private Color available = Color.white, notAvailable = Color.black;
        #endregion

        #region Properties
        public int ApCount
        {
            get
            {
                return apCount;
            }
            set
            {
                if(apCount == value)
                {
                    apCount = value;
                }
                else
                {
                    apCount = value;
                    Recalculate();
                }
            }
        }
        #endregion

        #region Events
        private void Awake()
        {
            apArray = new RectTransform[apCount];
        }
        #endregion

        #region Methods
        public void Recalculate()
        {
            float sizeX = Mathf.Abs(panel.sizeDelta.x / apCount);
            float sizeY = apBorder.sizeDelta.y;
            if(apArray.Length < apCount)
            {
                Array.Resize<RectTransform>(ref apArray, apCount);
            }
            int i;
            for(i = 0; i < apCount - 1; i++)
            {
                if(apArray[i] == null)
                {
                    apArray[i] = Instantiate(apBorder, panel.transform);
                }
                apArray[i].gameObject.SetActive(true);
                apArray[i].sizeDelta = new Vector2(sizeX, sizeY);
                apArray[i].anchoredPosition = new Vector2(sizeX * i, 0f);
                apArray[i].GetComponent<Image>().color = Color.white;
            }
            apArray[i] = apBorderEnd;
            apBorderEnd.sizeDelta = new Vector2(sizeX, sizeY);
            apBorderEnd.anchoredPosition = new Vector2(sizeX * (i), 0f);
            if(apCount < apArray.Length)
            {
                for(i = apCount - 1; i < apArray.Length - 1; i++)
                {
                    apArray[i].gameObject.SetActive(false);
                }
            }
        }

        public void SetActiveGroup(Group group)
        {
            if(activeGroup != null)
            {
                activeGroup.OnApChanged -= ApChange;
            }
            activeGroup = group;
            activeGroup.OnApChanged += ApChange;
            ApCount = activeGroup.MaxAP;
            ApChange(activeGroup.CurrentAP);
        }

        //Optimize?
        private void ApChange(int ap)
        {
            for(int i = 0; i < apArray.Length; i++)
            {
                if(i < ap)
                {
                    apArray[i].GetComponent<Image>().color = available;
                }
                else
                {
                    apArray[i].GetComponent<Image>().color = notAvailable;
                }
            }
        }
        #endregion
    }
}
