﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndTurnButton : MonoBehaviour {

    private Button button;
    private Image buttonImage;
    private bool pulsing = false;
    private Color originalColor;
    private Color targetColor;

	void Awake () {
        button = this.GetComponent<Button>();
        buttonImage = GetComponent<Image>();

        originalColor = buttonImage.color;
        Vector3 hsvColor;
        Color.RGBToHSV(originalColor, out hsvColor.x, out hsvColor.y, out hsvColor.z);
        hsvColor.z = hsvColor.z * 0.75f;
        targetColor = Color.HSVToRGB(hsvColor.x, hsvColor.y, hsvColor.z);
        targetColor.a = 0.75f;
    }

    public void SetInteractable(bool interactable)
    {
        if (interactable)
        {
            button.interactable = true;
        }
        else
        {
            button.interactable = false;
        }
    }

    public void Pulse(float singlePulseDuration, int repeat = -1)
    {
        StartCoroutine(PulseButton(singlePulseDuration, 0.75f, 1f, repeat));
    }

    public void StopPulse()
    {
        pulsing = false;
    }

    private IEnumerator PulseButton(float singlePulseDuration, float minAlpha, float maxAlpha, int repeat = -1)
    {
        pulsing = true;
        while (pulsing && (repeat == -1 || repeat < 1))
        {
            buttonImage.CrossFadeColor(targetColor, singlePulseDuration, false, true);
            yield return new WaitForSeconds(singlePulseDuration);
            buttonImage.CrossFadeColor(originalColor, singlePulseDuration, false, true);
            yield return new WaitForSeconds(singlePulseDuration);
        }
    }
}
