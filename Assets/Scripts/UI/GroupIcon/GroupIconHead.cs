﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace ProjectHunt
{
    public abstract class GroupIconHead : MonoBehaviour
    {
        #region Data

        protected Group group;

        protected UIController uiController;
        protected Color color;
        protected Image image;

        #endregion

        #region Events
        protected void Awake()
        {
            image = GetComponent<Image>();
        }
        #endregion

        #region Methods
        public virtual void Init(UIController uiController, Group group)
        {
            this.uiController = uiController;
            this.group = group;
        }

        public virtual void SetColor(Color color)
        {
            this.color = color;
            image.color = color;
        }
        #endregion
    }
}