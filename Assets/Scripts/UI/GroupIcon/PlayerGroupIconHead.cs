﻿using UnityEngine;
using UnityEngine.UI;

namespace ProjectHunt
{
    class PlayerGroupIconHead : GroupIconHead
    {
        #region Data
        private Button button;
        [SerializeField]
        private Image entityCount;
        [SerializeField]
        private Sprite normal, highlight;
        [SerializeField]
        private Sprite[] silhouetteSpriteArray;
        #endregion

        #region Events
        new void Awake()
        {
            base.Awake();
            button = GetComponent<Button>();
            button.onClick.AddListener(ForwardOnClick);
        }
        #endregion

        #region Methods
        public override void SetColor(Color color)
        {
            base.SetColor(color);
            entityCount.color = color;
        }

        public void SetEntityCount(int count)
        {
            entityCount.sprite = count > 0 ?
                (count < silhouetteSpriteArray.Length ?
                silhouetteSpriteArray[count - 1] : silhouetteSpriteArray[silhouetteSpriteArray.Length - 1])
                : null;
        }

        public void SetHighlight(bool highlighted)
        {
            if (highlighted)
            {
                image.sprite = highlight;
            }
            else
            {
                image.sprite = normal;
            }
        }

        public void SetInteractable(bool interactable)
        {
            if (interactable)
            {
                button.interactable = true;
                image.color = color;
                entityCount.color = color;
            }
            else
            {
                button.interactable = false;
                image.color = Color.white;
                entityCount.color = Color.white;
            }
        }

        private void ForwardOnClick()
        {
            uiController.GroupClicked(group.GroupID, group.MemberID);
        }
        #endregion
    }
}
