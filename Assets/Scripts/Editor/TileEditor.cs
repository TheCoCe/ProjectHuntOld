﻿using UnityEngine;
using UnityEditor;
using ProjectHunt;

[CustomEditor(typeof(GridTile), true)]
[CanEditMultipleObjects]
public class TileEditor : Editor
{
    #region Data

    SerializedProperty tileBiomeType;
    SerializedProperty isPath;
    SerializedProperty isBlocked;
    SerializedProperty isNest;
    SerializedProperty fieldevent;
    SerializedProperty movementCost;
    SerializedProperty aiCostModifier;
    SerializedProperty visionCost;
    SerializedProperty visionRangeModifier;
    SerializedProperty exploredOnStart;

    private static PrefabMap prefabMap;
    private static Transform tileParent;

    private static int cycleState;
    private bool editTile = false;
    private static bool editorFoldout = true;
    private static bool editFoldout = false;
    private static BiomeType biomeType;
    private static Tool lastTool;

    private bool showSelectionWindow = false;
    private Rect windowRect = new Rect(20, 20, 250, 50);
    string setName = "tileSet";
    #endregion

    private void OnEnable()
    {
        if (prefabMap == null)
        {
            prefabMap = PrefabMap.Instance;
        }

        if (tileParent == null)
        {
            MapEditorWindow.CreateTilesParent();
        }


        tileBiomeType = serializedObject.FindProperty("biomeType");
        isPath = serializedObject.FindProperty("isPath");
        isBlocked = serializedObject.FindProperty("blocked");
        isNest = serializedObject.FindProperty("isNest");
        fieldevent = serializedObject.FindProperty("fieldevent");
        movementCost = serializedObject.FindProperty("movementCost");
        aiCostModifier = serializedObject.FindProperty("aiCostModifier");
        visionCost = serializedObject.FindProperty("visionCost");
        visionRangeModifier = serializedObject.FindProperty("visionRangeModifier");
        exploredOnStart = serializedObject.FindProperty("explored");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        editTile = EditorGUILayout.Toggle("Edit Tile", editTile);
        EditorGUI.BeginDisabledGroup(!editTile);
        editFoldout = EditorGUILayout.Foldout(editFoldout, "Tile Settings");
        if (editFoldout)
        {
            EditorGUILayout.PropertyField(tileBiomeType);
            EditorGUILayout.PropertyField(isBlocked);
            EditorGUILayout.PropertyField(isPath);
            EditorGUILayout.PropertyField(isNest);
            EditorGUILayout.PropertyField(movementCost);
            EditorGUILayout.PropertyField(aiCostModifier);
            EditorGUILayout.PropertyField(visionCost);
            EditorGUILayout.PropertyField(visionRangeModifier);
            EditorGUILayout.PropertyField(exploredOnStart);
        }
        EditorGUI.EndDisabledGroup();

        DrawUILine(new Color(0.65f, 0.65f, 0.65f));

        EditorGUILayout.PropertyField(fieldevent);
        serializedObject.ApplyModifiedProperties();

        DrawUILine(new Color(0.65f, 0.65f, 0.65f));

        editorFoldout = EditorGUILayout.Foldout(editorFoldout, "Map Editor Tools");
        if (editorFoldout)
        {
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.BeginHorizontal();
            
            if(targets.Length > 1)
            {
                EditorGUILayout.LabelField("Center coords:", EditorStyles.largeLabel);
                GUILayout.FlexibleSpace();
                GridCoord coord = new GridCoord(0, 0);
                for (int i = 0; i < targets.Length; i++)
                    coord += (targets[i] as GridTile).Coord;
                coord.x = coord.x / targets.Length;
                coord.y = coord.y / targets.Length;

                EditorGUILayout.LabelField(((GridCubeCoord)coord).ToString(), EditorStyles.largeLabel);
            }
            else
            {
                EditorGUILayout.LabelField("Coordinates:", EditorStyles.largeLabel);
                GUILayout.FlexibleSpace();
                EditorGUILayout.LabelField((target as GridTile).Coord.ToString(), EditorStyles.largeLabel);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUI.EndDisabledGroup();
            ChangeTileType();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Randomize Rotation"))
            {
                RandomRotation();
            }

            if(GUILayout.Button("Reset Rotation"))
            {
                ResetRotation();
            }
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Save Selection"))
            {
                showSelectionWindow = true;
            }
        }

    }

    private void ChangeTileType()
    {
        GridTile t = targets[0] as GridTile;
        EditorGUI.BeginChangeCheck();
        biomeType = (BiomeType)EditorGUILayout.EnumPopup("Change Tile Type", t.biomeType);

        if (EditorGUI.EndChangeCheck() && prefabMap != null)
        {
            cycleState = 0;
            ReplaceObjects(prefabMap.GetListByType(biomeType));
        }

        int newCycleState = EditorGUILayout.Popup(cycleState, prefabMap.GetVariationsOfType(biomeType));
        if (cycleState != newCycleState)
        {
            cycleState = newCycleState;
            ReplaceObjects(prefabMap.GetListByType(biomeType), true);
        }
    }

    private void ReplaceObjects(GridTileList list, bool cycle = false)
    {
        //Undo.ClearAll();
        if (list == null || list.tiles.Count < 1)
        {
            Debug.LogWarning("Tile type " + biomeType + " has no Assets assigned or doesn't exist in the PrefabMap!");
            return;
        }
        GridTile actor;
        GameObject instance;
        Object[] selection = new Object[targets.Length];
        for (int i = 0; i < targets.Length; i++)
        {
            actor = targets[i] as GridTile;
            instance = Instantiate(list.tiles[cycleState].gameObject,
                (GridCoord)actor.Coord,
                actor.transform.rotation,
                actor.transform.parent);
            //TODO: Undo is extremely slow so I clear the undo history every time I register a undo
            //Maybe just remove undo support completely
            //Undo.RegisterCreatedObjectUndo(instance, "Replaced Object");
            //Undo.DestroyObjectImmediate(actor.gameObject);
            DestroyImmediate(actor.gameObject);
            selection[i] = instance;
        }
        UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
        Selection.objects = selection;
    }

    private void RandomRotation()
    {
        GridTile actor;
        for (int i = 0; i < targets.Length; i++)
        {
            actor = targets[i] as GridTile;
            actor.gameObject.transform.rotation = Quaternion.Euler(0f, ((int)(Random.Range(0f, 360f) / 60f) * 60f) % 360f, 0f);
        }
    }

    private void ResetRotation()
    {
        GridTile actor;
        for (int i = 0; i < targets.Length; i++)
        {
            actor = targets[i] as GridTile;
            actor.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }
    }   

    private void SelectionWindow(int windowId)
    {
        setName = GUILayout.TextField(setName, 20);
        if(GUILayout.Button("Confirm"))
        {
            SaveSet(setName);
            showSelectionWindow = false;
        }
        GUI.DragWindow(new Rect(0, 0, 10000, 10000));
    }

    private void SaveSet(string collectionName)
    {
        TileSet newSet = ScriptableObject.CreateInstance<TileSet>();
        for(int i = 0; i < targets.Length; i++)
            newSet.set.Add((targets[i] as GridTile).Coord);

        AssetDatabase.CreateAsset(newSet, "Assets/ScriptableObjects/TileSets/" + collectionName + ".asset");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    public void OnSceneGUI()
    {
        Tools.current = Tools.current == Tool.None ? (lastTool == Tool.Rotate ? Tool.Rotate : Tool.Move) : Tools.current; //Could add custom move gizmo
        GridTile actor = target as GridTile;

        if (actor != null)
        {
            actor.transform.parent = tileParent ? tileParent : actor.transform.parent;
            actor.transform.position = (GridCoord)actor.transform.position;

            if (Tools.current == Tool.Rotate)
            {
                Tools.current = Tool.None;
                lastTool = Tool.Rotate;
                Quaternion rotation = Handles.Disc(actor.transform.rotation, actor.transform.position, Vector3.up, 0.5f, false, 1f);
                actor.transform.rotation = rotation;

                Vector3 euler = actor.transform.rotation.eulerAngles;
                euler.y = ((int)(euler.y / 60f) * 60f) % 360f;
                actor.transform.rotation = Quaternion.Euler(euler);
            }
        }

        if(showSelectionWindow)
        {
            windowRect = GUILayout.Window(0, windowRect, SelectionWindow, "Name the Set");
        }
    }

    public static void DrawUILine(Color color, int thickness = 2, int padding = 10)
    {
        Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
        r.height = thickness;
        r.y += padding / 2;
        r.x -= 2;
        r.width += 6;
        EditorGUI.DrawRect(r, color);
    }
}