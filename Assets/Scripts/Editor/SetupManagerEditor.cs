﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

namespace ProjectHunt
{
    [CustomEditor(typeof(SetupData))]
    public class SetupManagerEditor : Editor
    {
        SetupData t;
        SerializedObject getTarget;
        SerializedProperty gameMembersList;
        List<bool> foldouts;
        List<bool> secondFoldout;
        GUIStyle boldLable;
        int listSize;

        private void OnEnable()
        {
            t = (SetupData)target;
            getTarget = new SerializedObject(t);
            foldouts = new List<bool>();
            secondFoldout = new List<bool>();
            gameMembersList = getTarget.FindProperty("gameMembers");
            boldLable = new GUIStyle();
            boldLable.fontStyle = FontStyle.Bold;
        }

        public override void OnInspectorGUI()
        {
            //Update the target
            getTarget.Update();

            //Update the GameMember list and foldout bools
            listSize = gameMembersList.arraySize;
            if(listSize != gameMembersList.arraySize)
            {
                while (listSize > gameMembersList.arraySize)
                {
                    gameMembersList.InsertArrayElementAtIndex(gameMembersList.arraySize);
                }
                while (listSize < gameMembersList.arraySize)
                {
                    gameMembersList.DeleteArrayElementAtIndex(gameMembersList.arraySize);
                }
            }
            if(foldouts.Count != gameMembersList.arraySize)
            {
                while (foldouts.Count < gameMembersList.arraySize)
                    foldouts.Add(false);
                while (foldouts.Count > gameMembersList.arraySize)
                    foldouts.RemoveAt(foldouts.Count - 1);
            }
            if(secondFoldout.Count != gameMembersList.arraySize)
            {
                while (secondFoldout.Count < gameMembersList.arraySize)
                    secondFoldout.Add(false);
                while (secondFoldout.Count > gameMembersList.arraySize)
                    secondFoldout.RemoveAt(secondFoldout.Count - 1);
            }

            EditorGUILayout.Space();

            //Add GameMember button
            if (GUILayout.Button("Add Game Member"))
            {
                t.gameMembers.Add(new GameMemberSetupData());
            }

            EditorGUILayout.Space();

            //Show GameMember list
            for(int i = 0; i < gameMembersList.arraySize; i++)
            {
                //Get GameMember properties
                SerializedProperty gameMemberSetupData = gameMembersList.GetArrayElementAtIndex(i);
                SerializedProperty isAI = gameMemberSetupData.FindPropertyRelative("isAI");

                //Check GameMember foldout
                foldouts[i] = EditorGUILayout.Foldout(foldouts[i], "Game Member " + i);
                if (foldouts[i])
                {
                    EditorGUILayout.BeginVertical(EditorStyles.helpBox);

                    //GameMember lable and remove button
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Game Member " + i);
                    if(GUILayout.Button("X", GUILayout.Width(20)))
                        t.gameMembers.RemoveAt(i);
                    EditorGUILayout.EndHorizontal();

                    DrawUILine(new Color(0.65f, 0.65f, 0.65f), 1, 5);

                    EditorGUI.indentLevel += 1;
                    EditorGUILayout.PropertyField(isAI);
                    
                    //Text style
                    GUIStyle style = EditorStyles.foldout;
                    style.fontStyle = FontStyle.Bold;
                    secondFoldout[i] = EditorGUILayout.Foldout(secondFoldout[i], "Groups", style);
                    style.fontStyle = FontStyle.Normal;

                    //Group foldout
                    if (secondFoldout[i])
                    {
                        //EditorGUI.indentLevel = 1;
                        SerializedProperty groupList = gameMemberSetupData.FindPropertyRelative("groups");
                        //Group list
                        int newCount = groupList.arraySize;
                        while (newCount < groupList.arraySize)
                        {
                            groupList.DeleteArrayElementAtIndex(groupList.arraySize - 1);
                        }
                        while (newCount > groupList.arraySize)
                        {
                            groupList.InsertArrayElementAtIndex(groupList.arraySize);
                        }
                        //Display Group Data
                        for(int j = 0; j < groupList.arraySize; j++)
                        {
                            SerializedProperty group = groupList.GetArrayElementAtIndex(j);
                            SerializedProperty spawnPoint = group.FindPropertyRelative("gridCoordSpawnPosition");
                            SerializedProperty startingBehaviour = group.FindPropertyRelative("startingBehaviour");
                            SerializedProperty maxAP = group.FindPropertyRelative("maxAP");
                            SerializedProperty currentAP = group.FindPropertyRelative("currentAP");
                            SerializedProperty entities = group.FindPropertyRelative("entities");
                            SerializedProperty spawnPointX = spawnPoint.FindPropertyRelative("x");
                            SerializedProperty spawnPointY = spawnPoint.FindPropertyRelative("y");
                            Vector2Int spawnVector = new Vector2Int(spawnPointX.intValue, spawnPointY.intValue);

                            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                            //Header
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField("Group " + j, boldLable);
                            if(GUILayout.Button("X", GUILayout.Width(20)))
                                t.gameMembers[i].groups.RemoveAt(j);
                            EditorGUILayout.EndHorizontal();
                            
                            //AP
                            EditorGUILayout.PropertyField(maxAP);
                            EditorGUILayout.PropertyField(currentAP);

                            Event evt = Event.current;

                            EditorGUI.BeginChangeCheck();
                            Rect dropRect = EditorGUILayout.BeginHorizontal();
                            spawnVector = EditorGUILayout.Vector2IntField("Spawn Position", spawnVector);
                            if (EditorGUI.EndChangeCheck())
                            {
                                spawnPointX.intValue = spawnVector.x;
                                spawnPointY.intValue = spawnVector.y;
                            }
                            EditorGUILayout.EndHorizontal();

                            //Handling Drag and Drop GridTile
                            switch (evt.type)
                            {
                                case UnityEngine.EventType.DragUpdated:
                                case UnityEngine.EventType.DragPerform:
                                    if (!dropRect.Contains(evt.mousePosition))
                                        break;

                                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                                    if (evt.type == UnityEngine.EventType.DragPerform)
                                    {
                                        DragAndDrop.AcceptDrag();
                                        foreach (GameObject dragged_object in DragAndDrop.objectReferences)
                                        {
                                            GameObject test = (dragged_object as GameObject);
                                            GridTile gridTile = test.GetComponent<GridTile>();
                                            if (test != null)
                                            {
                                                spawnVector.x = ((GridCoord)gridTile.Coord).x;
                                                spawnVector.y = ((GridCoord)gridTile.Coord).y;
                                                spawnPointX.intValue = spawnVector.x;
                                                spawnPointY.intValue = spawnVector.y;
                                            }
                                        }
                                    }
                                    break;
                            }

                            //Entities
                            EditorGUILayout.PropertyField(entities, true);

                            //AI Starting behaviour
                            EditorGUI.BeginDisabledGroup(!isAI.boolValue);
                            EditorGUILayout.PropertyField(startingBehaviour);
                            EditorGUI.EndDisabledGroup();

                            EditorGUILayout.Space();
                            EditorGUILayout.EndVertical();
                        }
                        //Add Buttons
                        GUILayout.BeginHorizontal();
                        GUILayout.FlexibleSpace();
                        if (GUILayout.Button("+", GUILayout.Width(20)))
                        {
                            t.gameMembers[i].groups.Add(new GroupSetupData());
                        }
                        if (GUILayout.Button("-", GUILayout.Width(20)))
                        {
                            t.gameMembers[i].groups.RemoveAt(t.gameMembers[i].groups.Count - 1);
                        }
                        GUILayout.EndHorizontal();
                    }
                    EditorGUI.indentLevel = 0;
                    EditorGUILayout.EndVertical();
                }
                if(i + 1 < gameMembersList.arraySize)
                    DrawUILine(Color.gray);
            }
            getTarget.ApplyModifiedProperties();
        }

        public static void DrawUILine(Color color, int thickness = 2, int padding = 10)
        {
            Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
            r.height = thickness;
            r.y += padding / 2;
            r.x -= 2;
            r.width += 6;
            EditorGUI.DrawRect(r, color);
        }
    }
}
