﻿using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    public class TileShaderData : MonoBehaviour
    {
        [SerializeField]
        public const float transitionSpeed = 255f;

        Texture2D tileTexture;
        Color32[] tileTextureData;

        List<GridTile> transitioningTiles = new List<GridTile>();

        public bool ImmediateMode { get; set; }

        public int Initialize(int tileCount)
        {
            Debug.Log("TileShaderData initialized");
            int sideLength = Mathf.NextPowerOfTwo(Mathf.CeilToInt(Mathf.Sqrt(tileCount)));

            if (tileTexture)
            {
                tileTexture.Resize(sideLength, sideLength);
            }
            else
            {
                tileTexture = new Texture2D(sideLength, sideLength, TextureFormat.RGBA32, false, true)
                {
                    filterMode = FilterMode.Point,
                    wrapModeU = TextureWrapMode.Repeat,
                    wrapModeV = TextureWrapMode.Clamp
                };
                Shader.SetGlobalTexture("_GridTileData", tileTexture);
            }
            Shader.SetGlobalVector(
                "_GridTileData_TexelSize",
                new Vector4(1f / sideLength, 1f / sideLength, sideLength, sideLength)
            );

            if (tileTextureData == null || tileTextureData.Length != Mathf.Pow(sideLength, 2.0f))
            {
                tileTextureData = new Color32[(int)Mathf.Pow(sideLength, 2.0f)];
            }
            else
            {
                for (int i = 0; i < tileTextureData.Length; i++)
                {
                    tileTextureData[i] = new Color32(0, 0, 0, 0);
                }
            }

            transitioningTiles.Clear();
            enabled = true;
            return sideLength;
        }

        public void RefreshVisibility(GridTile tile)
        {
            int id = tile.tileID;
            if (ImmediateMode)
            {
                tileTextureData[id].r = tile.IsVisible ? (byte)255 : (byte)0;
                tileTextureData[id].g = tile.IsExplored ? (byte)255 : (byte)0;
            }
            else if (tileTextureData[id].b != 255)
            {
                tileTextureData[id].b = 255;
                transitioningTiles.Add(tile);
            }
            enabled = true;
        }

        public void SetMapData(GridTile tile, float data)
        {
            tileTextureData[tile.tileID].b =
                data < 0f ? (byte)0 : (data < 1f ? (byte)(data * 254f) : (byte)254);
            enabled = true;
        }

        void LateUpdate()
        {
            int delta = (int)(Time.deltaTime * transitionSpeed);
            if (delta == 0)
            {
                delta = 1;
            }
            for (int i = 0; i < transitioningTiles.Count; i++)
            {
                if (!UpdateTileData(transitioningTiles[i], delta))
                {
                    transitioningTiles[i--] = transitioningTiles[transitioningTiles.Count - 1];
                    transitioningTiles.RemoveAt(transitioningTiles.Count - 1);
                }
            }
            
            tileTexture.SetPixels32(tileTextureData);
            tileTexture.Apply();
            enabled = transitioningTiles.Count > 0;
        }

        bool UpdateTileData(GridTile tile, int delta)
        {
            int id = tile.tileID;
            Color32 data = tileTextureData[id];
            bool stillUpdating = false;

            if (tile.IsExplored && data.g < 255)
            {
                stillUpdating = true;
                int t = data.g + delta;
                data.g = t >= 255 ? (byte)255 : (byte)t;
            }

            if (tile.IsVisible)
            {
                if (data.r < 255)
                {
                    stillUpdating = true;
                    int t = data.r + delta;
                    data.r = t >= 255 ? (byte)255 : (byte)t;
                }
            }
            else if (data.r > 0)
            {
                stillUpdating = true;
                int t = data.r - delta;
                data.r = t < 0 ? (byte)0 : (byte)t;
            }

            if (!stillUpdating)
            {
                data.b = 0;
            }
            tileTextureData[id] = data;
            return stillUpdating;
        }
    }
}