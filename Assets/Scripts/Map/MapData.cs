﻿using System.Collections.Generic;
using UnityEngine;
using ProjectHunt;
using System.Linq;
using System.Collections;

public class MapData : MonoBehaviour
{
    #region Data

    private static MapData instance;
    public static MapData Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<MapData>();
            return instance;
        }
    }
    /// <summary>
    /// Enable/Disable the restricted tile set globally.
    /// </summary>
    public static bool enableRestrictedCoords = false;

    public Transform tileContainer;

    private Dictionary<GridCubeCoord, GridTile> map;
    private Dictionary<GridCubeCoord, TileMetadata> metaDataMap;
    private HashSet<GridCoord> restrictToCoords;

    private List<GridCubeCoord> villageList;
    private List<GridCubeCoord> nestList;
    private TileShaderData tileShaderData;

    private GridCubeCoord[,] groupPositions;

    //Monster Tracks
    TrackManager trackManager;

    //Debug coordinates
    public TextMesh coordText;
    public bool showCoordText;
    #endregion

    #region Properties

    public int Count
    {
        get
        {
            return map.Count;
        }
    }

    #endregion

    #region Events
    private void Awake()
    {
        map = new Dictionary<GridCubeCoord, GridTile>();
        metaDataMap = new Dictionary<GridCubeCoord, TileMetadata>();
        restrictToCoords = new HashSet<GridCoord>();
        villageList = new List<GridCubeCoord>();
        nestList = new List<GridCubeCoord>();
        if (!(tileShaderData = GetComponent<TileShaderData>()))
            tileShaderData = gameObject.AddComponent<TileShaderData>();
        groupPositions = new GridCubeCoord[0, 0];
        trackManager = new TrackManager();
    }

    private void Start()
    {
        if (tileContainer == null)
            tileContainer = GameObject.FindWithTag("Tiles").transform;

        if (tileContainer != null)
        {
            GridTile[] tiles = tileContainer.GetComponentsInChildren<GridTile>();

            foreach (GridTile tile in tiles)
            {
                if (map.ContainsKey(tile.Coord))
                {
                    Debug.LogWarning("<color=red>Two Tiles with same coord at " +
                        tile.Coord.x + ", " + tile.Coord.y + "! Disabled the second one found:</color>");
                    tile.gameObject.SetActive(false);
                    continue;
                }
                //if (tile != null) //shouldn't be needed
                {
                    map.Add(tile.Coord, tile);
                    tile.tileID = map.Count - 1;

                    if (tile.biomeType == BiomeType.Village)
                        villageList.Add(tile.Coord);
                    if (tile.IsNest)
                        nestList.Add(tile.Coord);

                    if (showCoordText)
                    {
                        GameObject instance = Instantiate(coordText.gameObject, tile.transform);
                        instance.GetComponent<TextMesh>().text = tile.Coord.ToString();
                    }
                }
            }
            Debug.Log("Tile Count: " + map.Count);
        }
        else
            Debug.Log("No tileContainer with name found!");

        //Initialize TileShaderData and GridTiles
        int sideLength = tileShaderData.Initialize(map.Count);
        foreach (KeyValuePair<GridCubeCoord, GridTile> entry in map)
        {
            entry.Value.InitializeVertexUVs(sideLength);
            entry.Value.ShaderData = tileShaderData;
            entry.Value.ShaderData.RefreshVisibility(entry.Value);
        }
    }

    private void OnEnable()
    {
        GameMember.OnGroupCreated += GroupPositionArrayResize;
        GameMember.OnGroupCreated += InitializeVisibility;
        AIGameMember.OnAIGroupCreated += GroupPositionArrayResize;
        TurnManager.OnTurnEnd += UpdateGroupPositions;
    }

    private void OnDisable()
    {
        GameMember.OnGroupCreated -= GroupPositionArrayResize;
        GameMember.OnGroupCreated -= InitializeVisibility;
        AIGameMember.OnAIGroupCreated -= GroupPositionArrayResize;
        TurnManager.OnTurnEnd -= UpdateGroupPositions;
    }

    public delegate void VisibilityChangeDelegate();
    public static event VisibilityChangeDelegate OnVisibilityChanged;
    #endregion

    #region Methods
    /// <summary>
    /// Gets a tile at a coord.
    /// </summary>
    /// <param name="coord">The Coords to check at</param>
    /// <param name="tile">The tile found at the coord</param>
    /// <param name="ignoreRestrictedTiles">If true, restricted set will be ignored</param>
    /// <returns>Returns true if a tile was found, false if not.</returns>
    private bool TryGetTile(GridCubeCoord coord, out GridTile tile, bool ignoreRestrictedTiles = false)
    {
        tile = null;
        if (!ignoreRestrictedTiles && enableRestrictedCoords && !restrictToCoords.Contains(coord))
            return false;
        if (map.TryGetValue(coord, out tile))
            return true;
        return false;
    }

    /// <summary>
    /// Returns true if a tile was found
    /// </summary>
    /// <param name="coord">Coordinate to search a tile at</param>
    /// <param name="tile">Contains the tile after completing the method</param>
    /// <returns>Returns true if a tile was found false if not</returns>
    public bool GetTileAtCoord(GridCubeCoord coord, out GridTile tile, bool ignoreRestricted = true)
    {
        if (TryGetTile(coord, out tile, ignoreRestricted))
            return true;
        return false;
    }

    /// <summary>
    /// Returns the tile at the given coord
    /// </summary>
    /// <param name="coord">Coordinate of the potential tile</param>
    /// <returns>Returns the tile. Returns null if no tile was found</returns>
    public GridTile GetTileAtCoord(GridCubeCoord coord, bool ignoreRestricted = true)
    {
        GridTile tile;
        if (TryGetTile(coord, out tile, ignoreRestricted))
            return tile;
        return null;
    }

    /// <summary>
    /// Returns true if a tile at the given coordinate exists.
    /// </summary>
    /// <param name="coord">Coordinate of the potential tile</param>
    /// <returns>Returns true if there is a tile at the given coord. Returns false otherwise</returns>
    public bool IsValidTile(GridCubeCoord coord, bool ignoreRestricted = true)
    {
        if(ignoreRestricted || !enableRestrictedCoords)
        {
            if (map.ContainsKey(coord))
            {
                return true;
            }
            return false;
        }
        else
        {
            if(restrictToCoords.Contains(coord) && map.ContainsKey(coord))
            {
                return true;
            }
            return false;
        }
    }

    /// <summary>
    /// Returns an array with the neighbouring tiles
    /// </summary>
    /// <param name="coord">The tile coordinate</param>
    /// <param name="neighbours">An out parameter array the tiles are written to</param>
    public void GetNeighbours(GridCubeCoord coord, out GridTile[] neighbours, bool ignoreRestricted = true)
    {
        neighbours = new GridTile[6];
        for (int i = 0; i < 6; i++)
        {
            TryGetTile(coord + GridCubeCoord.neighborList[i], out neighbours[i], ignoreRestricted);
        }
    }


    /// <summary>
    /// Returns a list containing every tile within the radius around the given coord
    /// </summary>
    /// <param name="coord">Center coordinate</param>
    /// <param name="radius">Radius around the center coordinate</param>
    /// <returns>List of Grid Tiles</returns>
    public List<GridTile> GetTilesInRadius(GridCubeCoord coord, int radius, bool ignoreRestricted = true)
    {
        List<GridTile> list = new List<GridTile>();
        List<GridCubeCoord> coordList = GridCubeCoord.GetTilesInRadius(coord, radius);
        GridTile tile;

        for (int i = 0; i < coordList.Count; i++)
        {
            if (TryGetTile(coordList[i], out tile, ignoreRestricted))
            {
                list.Add(tile);
            }
        }

        return list;
    }

    public List<GridTile> GetVisibleTiles(GridCubeCoord coord, int radius)
    {
        List<GridTile> rim = new List<GridTile>();
        List<GridTile> visibleTiles = GetVisibleTiles(coord, radius, out rim);
        visibleTiles.AddRange(rim);
        return visibleTiles;
    }

    public List<GridTile> GetVisibleTiles(GridCubeCoord coord, int radius, out List<GridTile> rim)
    {
        rim = new List<GridTile>();
        List<GridTile> visibleTiles = new List<GridTile>();
        Queue<GridTile> frontier = new Queue<GridTile>();
        HashSet<GridCubeCoord> visitedSet = new HashSet<GridCubeCoord>();

        //Get the center Tile, ad it to the frontier and set its distance to the center to 0
        GridTile centerTile = GetTileAtCoord(coord);
        frontier.Enqueue(centerTile);
        centerTile.Distance = 0;

        radius += (centerTile.VisionRangeModifier - 1);

        while(frontier.Count > 0)
        {
            //Get the the next Tile to be examined and add it to the visited Tiles
            GridTile current = frontier.Dequeue();
            visitedSet.Add(current.Coord);
            visibleTiles.Add(current);

            GridTile[] neighbours;
            GetNeighbours(current.Coord, out neighbours);
            for (int i = 0; i < 6; i++)
            {
                //If neighbour not existes or was already visited continue with next neighbour
                if (neighbours[i] == null || visitedSet.Contains(neighbours[i].Coord))
                    continue;

                //ToDo: Comment
                int distance = current.Distance + 1;
                if (distance + neighbours[i].VisionModifier > radius
                    || distance > GridCubeCoord.GetDistance(centerTile.Coord, neighbours[i].Coord)
                )
                {
                    rim.Add(neighbours[i]);
                    continue;
                }

                neighbours[i].Distance = distance;
                frontier.Enqueue(neighbours[i]);
                visitedSet.Add(neighbours[i].Coord);
            }
        }
        return visibleTiles;
    }

    /// <summary>
    /// Returns closest village. Returns GridCubeCoord.Invalid if no village was found.
    /// </summary>
    /// <param name="position">Current group position</param>
    /// <returns></returns>
    public GridCubeCoord FindClosestVillageCoord(GridCubeCoord position)
    {
        return FindClosestFromList(villageList, position);
    }

    /// <summary>
    /// Returns clostest nest. Returns GridCubeCoord.invalid if no nest was found.
    /// </summary>
    /// <param name="position">Current group position</param>
    /// <returns></returns>
    public GridCubeCoord FindClosestNestCoord(GridCubeCoord position)
    {
        return FindClosestFromList(nestList, position);
    }

    /// <summary>
    /// Returns clostest group position. Returns GridCubeCoord.invalid if no group was found.
    /// </summary>
    /// <param name="position">Current group position</param>
    /// <param name="skipId">MemberID to ignore groups from</param>
    /// <returns></returns>
    public GridCubeCoord FindClosestPlayerCoord(GridCubeCoord position, uint skipId = uint.MaxValue, int radius = -1)
    {
        GridCubeCoord closest = GridCubeCoord.invalid;
        int distance = int.MaxValue;
        for(int i = 0; i < groupPositions.GetLength(0); i++)
        {
            if (i == skipId)
                continue;
            for(int j = 0; j < groupPositions.GetLength(1); j++)
            {
                if(GridCubeCoord.GetDistance(position, groupPositions[i, j]) < distance)
                {
                    distance = GridCubeCoord.GetDistance(position, groupPositions[i, j]);
                    if (radius > 0 && distance >= radius)
                        continue;
                    closest = groupPositions[i, j];
                }
            }
        }
        return closest;
    }

    /// <summary>
    /// Checks for groups at the given coordinate and returns success and a list with groups.
    /// </summary>
    /// <param name="coord">The coordinate to check at.</param>
    /// <param name="list">The list of groups found at coord.</param>
    /// <returns>true if groups were found, else false.</returns>
    public bool GetGroupsAtCoord(GridCubeCoord coord, out List<Group> list, int ignoreGameMemberID = -1)
    {
        TileMetadata tempMetadata;
        if (metaDataMap.TryGetValue(coord, out tempMetadata) && tempMetadata.GroupCount > 0)
        {
            list = tempMetadata.GroupList;
            if(ignoreGameMemberID >= 0)
            {
                List<Group> newList = new List<Group>();
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].MemberID != ignoreGameMemberID)
                        newList.Add(list[i]);
                }
                list = newList;
            }
            return true;
        }
        list = null;
        return false;
    }

    /// <summary>
    /// Returns the entry with shortest distance to position from the list. Returns GridCubeCoord.Invalid if list is empty.
    /// </summary>
    /// <param name="list">List to search from</param>
    /// <param name="position">Position to get distance to list entries</param>
    /// <returns></returns>
    private GridCubeCoord FindClosestFromList(List<GridCubeCoord> list, GridCubeCoord position)
    {
        if (list.Count <= 0)
            return GridCubeCoord.invalid;
        else if (list.Count == 1)
            return list[0];
        else
        {
            int distance;
            GridCubeCoord closest;

            closest = list[0];
            distance = GridCubeCoord.GetDistance(list[0], position);

            for (int i = 1; i < list.Count; i++)
            {
                if (GridCubeCoord.GetDistance(list[i], position) < distance)
                    closest = list[i];
            }
            return closest;
        }
    }

    /// <summary>
    /// Tries to destroy village at coord. Returns true if successful.
    /// </summary>
    /// <param name="position">Position to destroy village at</param>
    /// <returns></returns>
    public bool DestroyVillage(GridCubeCoord position)
    {
        if (villageList.Remove(position))
        {
            ExternalEventsController.Instance.AddExternalEvent(ExternalEventType.VillageDestroyed, null, true, position);
            Debug.Log("<color=red>Village at " + position + " destroyed!</color>");
            if(villageList.Count < 1)
            {
                UITextController.Instance.QueueTextBox(textBoxPosition.middleCenter, null, "Das Monster hat zu viele Leute angegriffen. Es ist zu gefährlich es weiter zu verfolgen. GAME OVER!", 0);
                StartCoroutine(GameOver());
            }
            return true;
        }
        return false;
    }

    /// <summary>
    /// TODO: MAKE NICE. This is to go back to main menu if the player looses
    /// </summary>
    /// <returns></returns>
    public IEnumerator GameOver()
    {
        yield return new WaitForSeconds(10f);
        UIController.Instance.MainMenuButton();
    }

    /// <summary>
    /// Updates the groupPositions array
    /// </summary>
    public void UpdateGroupPositions()
    {
        uint column = TurnManager.Instance.ActiveGameMember.MemberID;
        GridCubeCoord[] newGroupPositions = TurnManager.Instance.ActiveGameMember.GetGroupPositions();

        TileMetadata tempTileMetadataRemove = null, tempTileMetadataAdd;
        Group tempGroup = null;
        for (int i = 0; i < groupPositions.GetLength(1) && i < newGroupPositions.Length; i++)
        {
            if(groupPositions[column, i] != newGroupPositions[i]) {
                //Remove group from old tile
                if (metaDataMap.TryGetValue(groupPositions[column, i], out tempTileMetadataRemove))
                {
                    tempGroup = tempTileMetadataRemove.RemoveGroupById(column, (uint)i);
                    if (tempTileMetadataRemove.GroupCount != 0)
                        tempTileMetadataRemove = null;

                    Debug.Assert(tempGroup != null, "Failed to find group in MetaData groupList!");
                }

                //Add group to new tile
                if(metaDataMap.TryGetValue(newGroupPositions[i], out tempTileMetadataAdd))
                {
                    tempTileMetadataAdd.AddGroup(tempGroup);
                    if (tempTileMetadataRemove != null)
                        metaDataMap.Remove(groupPositions[column, i]);
                }
                else
                {
                    if(tempTileMetadataRemove == null)
                    {
                        tempTileMetadataAdd = new TileMetadata();
                    }
                    else
                    {
                        metaDataMap.Remove(groupPositions[column, i]);
                        tempTileMetadataAdd = tempTileMetadataRemove;
                    }
                    tempTileMetadataAdd.AddGroup(tempGroup);
                    metaDataMap.Add(newGroupPositions[i], tempTileMetadataAdd);
                }
                groupPositions[column, i] = newGroupPositions[i];
            }
        }
    }

    /// <summary>
    /// Resizes the groupPositions array and adds group positioon if necessary
    /// </summary>
    /// <param name="group">Group to add</param>
    private void GroupPositionArrayResize(Group group)
    {
        if (group.MemberID > groupPositions.GetLength(0) - 1 ||
           group.GroupID > groupPositions.GetLength(1) - 1)
        {
            ArrayResize.ResizeArray(
                ref groupPositions, 
                Mathf.Max((int)group.MemberID + 1, groupPositions.GetLength(0)), 
                Mathf.Max((int)group.GroupID + 1, groupPositions.GetLength(1))
                );
            groupPositions[group.MemberID, group.GroupID] = group.Position;
        }

        TileMetadata tempMetadata;
        if(metaDataMap.TryGetValue(group.Position, out tempMetadata))
        {
            tempMetadata.AddGroup(group);
        }
        else
        {
            tempMetadata = new TileMetadata();
            tempMetadata.AddGroup(group);
            metaDataMap.Add(group.Position, tempMetadata);
        }
    }

    /// <summary>
    /// Initilizes the visibility for a group on the map (on creation or start)
    /// </summary>
    /// <param name="group"></param>
    private void InitializeVisibility(Group group)
    {
        IncreaseVisibility(group.Position, group.ViewDistance);
    }

    /// <summary>
    /// Increases the visibility of Tiles in range around the center coord (breadth filled)
    /// Invokes the OnVisibilityChange event
    /// </summary>
    /// <param name="center"></param>
    /// <param name="range"></param>
    public void IncreaseVisibility(GridCubeCoord center, int range)
    {
        List<GridTile> rim = new List<GridTile>();
        //List<GridTile> Tiles = GetVisibleTiles(center, range, out rim);
        List<GridTile> Tiles = GetVisibleTiles(center, range);
        for (int i = 0; i < Tiles.Count; i++)
        {
            Tiles[i].IncreaseVisibility();
        }
        //Explore(rim);
        Explore(center, range);
        OnVisibilityChanged?.Invoke();
    }

    /// <summary>
    /// Decreases the visibility of Tiles in range around the center coord (breadth filled)
    /// </summary>
    /// <param name="center"></param>
    /// <param name="range"></param>
    public void DecreaseVisibility(GridCubeCoord center, int range)
    {
        List<GridTile> rim = new List<GridTile>();
        //List<GridTile> Tiles = GetVisibleTiles(center, range, out rim);
        List<GridTile> Tiles = GetVisibleTiles(center, range);
        for (int i = 0; i < Tiles.Count; i++)
        {
            Tiles[i].DecreaseVisibility();
        }
        //OnVisibilityChanged?.Invoke();
    }

    /// <summary>
    /// Explore Tiles in range around center coord (breadth filled)
    /// </summary>
    /// <param name="center"></param>
    /// <param name="range"></param>
    public void Explore(GridCubeCoord center, int range)
    {
        List<GridTile> Tiles = GetTilesInRadius(center, range);
        for (int i = 0; i < Tiles.Count; i++)
        {
            Tiles[i].IsExplored = true;
        }
    }

    /// <summary>
    /// Explores Tiles in list
    /// </summary>
    /// <param name="tiles"></param>
    public void Explore (List<GridTile> tiles)
    {
        for (int i = 0; i < tiles.Count; i++)
        {
            tiles[i].IsExplored = true;
        }
    }

    /// <summary>
    /// Adds new Tracks to the TrackManager on the path.
    /// </summary>
    /// <param name="path">The path to create the tracks on.</param>
    /// <param name="lifetime">Average lifetime of a track in rounds.</param>
    /// <param name="decayPerRound">The value the liftime is reduced by every round</param>
    /// <param name="randomLifetimeDelta">Maximum random offset for the lifetime. Range from 0 - randomLifetimeDelta will be added or remove from lifetime.</param>
    public void AddTracks(Queue<GridTile> path, ushort lifetime = 10, ushort decayPerRound = 1, ushort randomLifetimeDelta = 2)
    {
        trackManager.AddTracks(path, lifetime, decayPerRound, randomLifetimeDelta);
    }

    /// <summary>
    /// Checks for a Track at the given Coordinate.
    /// </summary>
    /// <param name="coord">The coordinate to check at</param>
    /// <returns></returns>
    public List<TrackManager.Track> CheckForTrack(GridCubeCoord coord)
    {
        return trackManager.CheckForTrack(coord);
    }

    /// <summary>
    /// Enables a hidden Track at the given coordinate. Will return true if a Track was found. False if not or the track is already enabled.
    /// </summary>
    /// <param name="coord">The coordinate to enable the track at.</param>
    /// <returns></returns>
    public bool ShowTracks(GridCubeCoord coord)
    {
        return trackManager.ShowTracks(coord);
    }

    /// <summary>
    /// Checks for a Monster Nest at the given Coordinate. Will return true if a nest was found at the given coordinate.
    /// </summary>
    /// <param name="coord">The coordinate to check at.</param>
    /// <returns></returns>
    public bool CheckForNest(GridCubeCoord coord)
    {
        GridCubeCoord nest = FindClosestNestCoord(coord);
        if (GridCubeCoord.GetDistance(coord, nest) <= 1)
        {
            //GameObject icon = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/UI/NestIconCanvas.prefab");
            GameObject icon = Resources.Load<GameObject>("NestIcon/NestIconCanvas");
            GameObject Quest = Instantiate(icon, (GridCoord)nest, icon.transform.rotation);
            Quest.transform.position = (GridCoord)nest + new Vector3(0f, 1f, 0f);
            return true;
        }
        return false;
    }

    /// <summary>
    /// Adds a single tile to the restricted HashSet
    /// </summary>
    /// <param name="coord">The coordinate to add</param>
    public void AddTileToRestrictedCoords(GridCubeCoord coord)
    {
        restrictToCoords.Add(coord);
    }

    /// <summary>
    /// Removes a single tile form the restricted HasSet
    /// </summary>
    /// <param name="coord">The coordinate to remove</param>
    /// <returns></returns>
    public bool RemoveTileFromRestrictedCoords(GridCubeCoord coord)
    {
        return restrictToCoords.Remove(coord);
    }

    /// <summary>
    /// Adds every value in Collection to the restricted Coords if it's not already in it.
    /// </summary>
    /// <param name="coords">A collection of coordinates to add</param>
    public void AddRestrictedCoords(IEnumerable<GridCubeCoord> coords)
    {
        foreach (var item in coords)
        {
            restrictToCoords.Add(item);
        }
    }

    /// <summary>
    /// Removes every value in the Collection from the restricted Coords if it contains the value.
    /// </summary>
    /// <param name="coords">A collection of coordinates to remove</param>
    public void RemoveRestrictedCoords(IEnumerable<GridCubeCoord> coords)
    {
        foreach (var item in coords)
        {
            restrictToCoords.Remove(item);
        }
    }

    /// <summary>
    /// Sets the restricted coords to the given HashSet
    /// </summary>
    /// <param name="coords">A HashSet containing the restricted coords</param>
    public void SetRestrictedCoords(HashSet<GridCoord> coords)
    {
        restrictToCoords = coords;
    }
    #endregion
}
