﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    public enum AStarMode
    {
        ShortestPath, ShortestPathWithRoads, WeightedPath, WeightedPathWithRoads
    }

    public class Pathfinding : MonoBehaviour
    {
        #region Data

        private MapData mapData;
        private PathRequestManager requestManager;

        public Transform seeker, target;

        private Heatmap currentHeatmap;
#if UNITY_DEBUG
        private List<GridTile> debugTileList;
#endif
        public GameObject coordText;

        bool foundPath = false;
        public bool debug = false;
#endregion

#region Events

        private void Awake()
        {
            mapData = GetComponent<MapData>();
            requestManager = GetComponent<PathRequestManager>();
#if UNITY_DEBUG
            debugTileList = new List<GridTile>();
#endif
        }

#endregion

#region Methods

        public void StartFindPathAStar(GridCubeCoord startPos, GridCubeCoord targetPos, AStarMode aStarMode, AIGroup group = null)
        {
            currentHeatmap = null;
            currentHeatmap = (TurnManager.Instance.ActiveGameMember as AIGameMember)?.Heatmap;
            StartCoroutine(AStar(startPos, targetPos, aStarMode));
        }

        IEnumerator AStar(GridCubeCoord startCoord, GridCubeCoord targetCoord, AStarMode aStarMode)
        {
#if UNITY_DEBUG
            foreach(GridTile tempTile in debugTileList)
            {
                tempTile.GetComponent<Renderer>().material.color = Color.white;
            }
#endif
            GridTile startTile, targetTile;
            if (!mapData.GetTileAtCoord(startCoord, out startTile) || !mapData.GetTileAtCoord(targetCoord, out targetTile))
            {
                requestManager.FinishedProcessingPath(null, false);
                yield break;
            }

            foundPath = false;

            if (startTile.IsWalkable && targetTile.IsWalkable)
            {
                Heap<GridTile> openSet = new Heap<GridTile>(mapData.Count);
                HashSet<GridTile> closedSet = new HashSet<GridTile>();
                openSet.Add(startTile);
#if UNITY_DEBUG
                startTile.GetComponent<Renderer>().material.color = Color.green;
                debugTileList.Add(startTile);
#endif
                while (openSet.Count > 0)
                {
                    GridTile tile = openSet.RemoveFirst();
                    closedSet.Add(tile);
#if UNITY_DEBUG
                    tile.GetComponent<Renderer>().material.color = Color.red;
                    if (!debugTileList.Contains(tile))
                    {
                        debugTileList.Add(tile);
                    }
#endif
                    if (tile == targetTile)
                    {
                        foundPath = true;
                        for(int i = 0; i < openSet.Count; i++)
                        {
                            tile = openSet.RemoveFirst();
                            tile.gCost = tile.hCost = 0;
                        }
                        foreach (GridTile item in closedSet)
                        {
                            item.gCost = item.hCost = 0;
                        }
                        break;
                    }

                    GridTile[] neighbours;
                    mapData.GetNeighbours(tile.Coord, out neighbours);

                    //Random Parameters for Path Selection
                    int rand = Random.Range(0, neighbours.Length);
                    bool bRand = Random.value < 0.5 ? true : false;

                    for (int i = 0; i < neighbours.Length; i++)
                    {
                        //Shift neighbour array indices by random number
                        int j = (i + rand) % (neighbours.Length);
                        
                        //Change iteration direction of the neighbours array
                        if (bRand)
                            j = neighbours.Length - j - 1;

                        if (neighbours[j] == null || !neighbours[j].IsWalkable || closedSet.Contains(neighbours[j]))
                            continue;

                        //Update neighbour[j] and add it to openSet
                        int newCostToNeighbour = 0;

                        switch (aStarMode)
                        {
                            case AStarMode.ShortestPathWithRoads:
                                newCostToNeighbour = tile.gCost + 
                                    GridCubeCoord.GetDistance(tile.Coord, neighbours[j].Coord) + 
                                    (neighbours[j].IsPath ? 0 : PrefabMap.Instance.notPathPenalty);
                                break;
                            case AStarMode.WeightedPath:
                                newCostToNeighbour = tile.gCost +
                                    GridCubeCoord.GetDistance(tile.Coord, neighbours[j].Coord) +
                                    neighbours[j].MovementCost * 2 +
                                    (currentHeatmap != null ? currentHeatmap.TryGetWeight(neighbours[j].Coord) : 0); //Add AStarMode if heatmap == null so we don't have to nullcheck?
                                break;
                            case AStarMode.WeightedPathWithRoads:
                                newCostToNeighbour = tile.gCost +
                                    GridCubeCoord.GetDistance(tile.Coord, neighbours[j].Coord) +
                                    neighbours[j].MovementCost * 2 +
                                    (neighbours[j].IsPath ? 0 : PrefabMap.Instance.notPathPenalty) +
                                    (currentHeatmap != null ? currentHeatmap.TryGetWeight(neighbours[j].Coord) : 0); //Add AStarMode if heatmap == null so we don't have to nullcheck?
                                break;
                            case AStarMode.ShortestPath:
                            default:
                                newCostToNeighbour = tile.gCost + GridCubeCoord.GetDistance(tile.Coord, neighbours[j].Coord);
                                break;
                        }

                        if (newCostToNeighbour < neighbours[j].gCost || !openSet.Contains(neighbours[j]))
                        {
                            neighbours[j].gCost = newCostToNeighbour;
                            neighbours[j].hCost = GridCubeCoord.GetDistance(neighbours[j].Coord, targetCoord);
                            neighbours[j].tileParent = tile;
#if UNITY_DEBUG
                            if(neighbours[j].textMesh == null)
                            {
                                GameObject instance = Instantiate(coordText.gameObject, neighbours[j].transform);
                                neighbours[j].textMesh = instance.GetComponent<TextMesh>();
                                neighbours[j].textMesh.transform.localScale = new Vector3(1f, 1f, 1f);
                                Vector3 rotation = new Vector3(90f, -neighbours[j].textMesh.transform.rotation.y, 0f);
                                neighbours[j].textMesh.transform.rotation = Quaternion.Euler(rotation);
                            }
                            neighbours[j].textMesh.text = "g: " + neighbours[j].gCost
                                + " h: " + neighbours[j].hCost
                                + " f: " + neighbours[j].FCost;
#endif

                            if (!openSet.Contains(neighbours[j]))
                            {
#if UNITY_DEBUG
                                if (!debugTileList.Contains(neighbours[j]))
                                {
                                    debugTileList.Add(neighbours[j]);
                                }
                                neighbours[j].GetComponent<Renderer>().material.color = Color.green;
#endif
                                openSet.Add(neighbours[j]);
                            }
                            else
                            {
                                openSet.UpdateItem(neighbours[j]);
#if UNITY_DEBUG
                                neighbours[j].textMesh.text = "g: " + neighbours[j].gCost
                                + " h: " + neighbours[j].hCost
                                + " f: " + neighbours[j].FCost;
#endif
                            }
                        }
#if UNITY_DEBUG
                        yield return new WaitForSeconds(0.05f);
#endif
                    }
                }               
            }

            yield return null;

            if(foundPath)
            { 
                requestManager.FinishedProcessingPath(RetracePathToTiles(startTile, targetTile), foundPath);
            }
            else
            {
                requestManager.FinishedProcessingPath(null, foundPath);
            }
        }

        //private GridCubeCoord[] RetracePathToCoords(GridTile startTile, GridTile endTile)
        //{
        //    List<GridCubeCoord> path = new List<GridCubeCoord>();
        //    GridTile currentTile = endTile;
        //
        //    while (currentTile != startTile)
        //    {
        //        path.Add(currentTile.Coord);
        //        currentTile = currentTile.tileParent;
        //    }
        //    path.Reverse();
        //    return path.ToArray();
        //}

        private List<GridTile> RetracePathToTiles(GridTile startTile, GridTile endTile)
        {
            List<GridTile> path = new List<GridTile>();
            GridTile currentTile = endTile;

            while (currentTile != startTile)
            {
#if UNITY_DEBUG
                currentTile.GetComponent<Renderer>().material.color = Color.yellow;
#endif
                path.Add(currentTile);
                currentTile = currentTile.tileParent;
            }
            path.Reverse();
            return path;
        }
#endregion
    }
}