﻿using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    public class TileSet : ScriptableObject
    {
        #region Data
        public List<GridCoord> set = new List<GridCoord>();
        #endregion
    }
}