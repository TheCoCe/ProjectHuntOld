﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor.SceneManagement;

namespace ProjectHunt
{

    [CreateAssetMenu(fileName = "SetupData", menuName = "Project Hunt/SetupData", order = 1)]
    public class SetupData : ScriptableObject
    {
        [Header("GameMember Setup Data")]
        public List<GameMemberSetupData> gameMembers;
    }

    [System.Serializable]
    public class GameMemberSetupData
    {
        [Tooltip("Specifies if the GameMember is controller by AI or by a Player")]
        public bool isAI;
        [Tooltip("The List of Groups that will be spawned when loading the map")]
        public List<GroupSetupData> groups = new List<GroupSetupData>();
    }

    [System.Serializable]
    public class GroupSetupData
    {
        [Tooltip("The List of Entities to spawn the group with")]
        public List<EntitySetupData> entities = new List<EntitySetupData>();
        [Tooltip("The Position to spawn the group at")]
        public GridCoord gridCoordSpawnPosition;
        public AIBehaviourType startingBehaviour;
        public int maxAP;
        public int currentAP;
    }

    [System.Serializable]
    public class EntitySetupData
    {
        public Entity entity;
    }
}
