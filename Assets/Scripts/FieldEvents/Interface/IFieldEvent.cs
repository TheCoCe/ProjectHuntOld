﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProjectHunt;

public interface IFieldEvent
{
    string DisplayText { get; }
    long LifeTime { get; }

    int MinGroupSize { get; }

    List<ProjectHunt.EventType> EventTypes { get; }

    void OnEnter(Group group);
    void OnExit();
}
