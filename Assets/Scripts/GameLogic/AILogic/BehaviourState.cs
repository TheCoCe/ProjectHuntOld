﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum AIBehaviourType
{
    RandomRoaming,
    RoamAroundPosition,
    GoToNest,
    GoToVillage,
    DebugMoveNorth,
    InitiateFlee,
    Flee,
    Aggressive
}

namespace ProjectHunt
{
    public abstract class BehaviourState
    {

        #region Data

        protected Queue<GridTile> finalPath;
        protected List<GridTile> leftOverPath;
        protected AIGroup myGroup;
        protected static int playerReactionDistance = 3;
        protected static int villageReactionDistance = 5;
        protected bool pathCreationInProgress = false;

        #endregion

        #region Properties

        public Queue<GridTile> Path
        {
            get
            {
                return finalPath;
            }
            set
            {
                finalPath = value;
            }
        }

        #endregion

        #region Methods
        protected abstract void CreatePath(List<GridTile> path, bool success);

        public virtual void HandleTurn()
        {
            pathCreationInProgress = true;
            PlayerInRange();
            VillageInRange();
            CreatePath(leftOverPath, true);
        }

        protected virtual bool PlayerInRange()
        {
            GridCubeCoord closestPlayer = MapData.Instance.FindClosestPlayerCoord(myGroup.Position, myGroup.MemberID, playerReactionDistance);
            if (closestPlayer != GridCubeCoord.invalid)
            {
                //Modify heatmap, discard remaining path and create a new one
                (TurnManager.Instance.GetGameMemberByID(myGroup.MemberID) as AIGameMember)?.Heatmap.AddToHeatmap(closestPlayer);
                return true;
            }
            return false;
        }

        protected virtual bool VillageInRange()
        {
            GridCubeCoord closestVillage = MapData.Instance.FindClosestVillageCoord(myGroup.Position);
            if(closestVillage != GridCubeCoord.invalid && GridCubeCoord.GetDistance(myGroup.Position, closestVillage) <= villageReactionDistance)
            {
                (TurnManager.Instance.GetGameMemberByID(myGroup.MemberID) as AIGameMember)?.Heatmap.AddToHeatmap(closestVillage);
                return true;
            }
            return false;
        }

        protected virtual GridCubeCoord GetPlayerInRangePosition(bool useRectionDistance = true, int modifyReactionDinstance = 0)
        {
            return MapData.Instance.FindClosestPlayerCoord(
                myGroup.Position, 
                myGroup.MemberID, 
                useRectionDistance ? playerReactionDistance + modifyReactionDinstance : -1
                );
        }

        protected virtual void ChangeBehaviour(AIBehaviourType newBehaviour)
        {
            BehaviourState newState = null;
            switch (newBehaviour)
            {
                case AIBehaviourType.RandomRoaming:
                    newState = new BehaviourRandomRoaming();
                    break;
                case AIBehaviourType.RoamAroundPosition:
                    newState = new BehaviourRoamingAroundPosition();
                    break;
                case AIBehaviourType.GoToNest:
                    newState = new BehaviourGoToNest();
                    break;
                case AIBehaviourType.GoToVillage:
                    newState = new BehaviourGoToVillage();
                    break;
                case AIBehaviourType.DebugMoveNorth:
                    newState = new BehaviourDebugMoveNorth();
                    break;
                case AIBehaviourType.InitiateFlee:
                    newState = new BehaviourInitiateFlee();
                    break;
                case AIBehaviourType.Flee:
                    newState = new BehaviourFlee();
                    break;
                case AIBehaviourType.Aggressive:
                    newState = new BehaviourAggressive();
                    break;
                default:
                    newState = new BehaviourRandomRoaming();
                    Debug.Log("<color=green>enum newBehaviour was invalid!</color>");
                    break;
            }
            Debug.Log("<color=green>Switching Behaviour to: " + newState.GetType() + "</color>");
            newState.myGroup = this.myGroup;
            newState.Path = this.Path;
            newState.pathCreationInProgress = this.pathCreationInProgress;
            myGroup.Behaviour = newState;
            if(pathCreationInProgress)
                myGroup.Behaviour.HandleTurn();
        }

        /// <summary>
        /// Call this if you want to Move. If you want the path to be continued next turn, call the overloaded Move method!
        /// </summary>
        protected virtual void Move()
        {
            pathCreationInProgress = false;
            //TODO: Do we want to go through the UIController, AIGameMember or directly through the AIGroup
            (TurnManager.Instance.GetGameMemberByID(myGroup.MemberID) as AIGameMember).AIGroupReady(myGroup.GroupID);
        }

        /// <summary>
        /// Call this if you want the current path to be continued next turn!
        /// </summary>
        /// <param name="path">The path you got from Pathfinding</param>
        /// <param name="completedTiles">The amount of tiles you traversed already</param>
        protected virtual void Move(List<GridTile> path, int completedTiles)
        {
            path.RemoveRange(0, completedTiles);
            leftOverPath = path;
            Move();
        }
        #endregion
    }
}