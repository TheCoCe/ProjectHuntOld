﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    public class AIGroup : Group
    {
        #region Data

        public delegate void ReadyToMove(Queue<GridTile> path);
        public ReadyToMove rtmDelegate;
        private BehaviourState behaviour;

        #endregion
        
        #region Properties

        public BehaviourState Behaviour
        {
            get
            {
                return behaviour;
            }
            set
            {
                behaviour = value;
            }
        }

        #endregion
   
        #region Methods
        public void Init(uint groupID, uint memberID, Entity[] newMembers, GridCubeCoord position, int currentAP = -1, int maxAP = -1, AIBehaviourType startingBehaviour = AIBehaviourType.RandomRoaming)
        {
            base.Init(groupID, memberID, newMembers, position, currentAP, maxAP);
            Behaviour = new BehaviourStartState(this, startingBehaviour);
        }

        protected override void Move(GridCubeCoord newPosition)
        {
            Position = newPosition;
        }

        public void PlanGroupTurn()
        {
            Behaviour.HandleTurn();
        }

        public void ExecuteGroupTurn()
        {
            rtmDelegate(behaviour.Path);
        }

        public void EndGroupTurn()
        {
            //TODO: reevaluate if we want to hold the member in the group as well
            (TurnManager.Instance.GetGameMemberByID(this.MemberID) as AIGameMember)?.AIGroupDone(this.GroupID);
        }
        #endregion
    }
}
