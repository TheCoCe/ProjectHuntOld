﻿using System;
using System.Collections.Generic;
using UnityEngine;
namespace ProjectHunt
{
    public class BehaviourRandomRoaming : BehaviourState {
        
        private GridCoord destination;
        private static int maxRoamDistance = 5;
        private static int minVillageDistance = 5;
        private int agressiveCooldown = -1;

        protected override void CreatePath(List<GridTile> path, bool success)
        {
            if(success && path != null)
            {
                for (int i = 0; i < path.Count; i++)
                {
                    if (myGroup.AttemptMove(path[i].Coord, path[i].MovementCost))
                        Path.Enqueue(path[i]);
                    else
                    {
                        Move();
                        return;
                    }

                    //Arrival Check
                    if (path[i].Coord == (GridCubeCoord)destination)
                    {
                        //Update player in range
                        PlayerInRange();
                        //Then move to finish
                        Move();
                        return;
                    }

                    //Player Check
                    if(PlayerInRange())
                    {
                        if(myGroup.AverageHealth > myGroup.AverageMaxHealth / 3)
                        { //HP > 1/3 * MaxHP
                            List<Group> tempGroup;
                            MapData.Instance.GetGroupsAtCoord(GetPlayerInRangePosition(), out tempGroup, (int)myGroup.MemberID);
                            if(tempGroup.Count == 1)
                            { //1 group
                                int random = UnityEngine.Random.Range(0, tempGroup[0].Size);
                                if(random == 0)
                                {
                                    ChangeBehaviour(AIBehaviourType.Aggressive);
                                    return;
                                }
                            }
                        }

                        PathRequestManager.RequestPath(path[i].Coord, destination, CreatePath, AStarMode.WeightedPath);
                        return;
                    }

                    if (GridCubeCoord.GetDistance(MapData.Instance.FindClosestVillageCoord(path[i].Coord), path[i].Coord) <= minVillageDistance)
                    {
                        ChangeBehaviour(AIBehaviourType.GoToVillage);
                        return;
                    }
                }
            }

            GridTile gridTile;
            do
            {
                destination.x = ((GridCoord)myGroup.Position).x + UnityEngine.Random.Range(-maxRoamDistance, maxRoamDistance);
                destination.y = ((GridCoord)myGroup.Position).y + UnityEngine.Random.Range(-maxRoamDistance, maxRoamDistance);
            } while (!(MapData.Instance.GetTileAtCoord(destination, out gridTile)) || gridTile.IsWalkable == false);

            Path.Enqueue(MapData.Instance.GetTileAtCoord(myGroup.Position));
            PathRequestManager.RequestPath(myGroup.Position, destination, CreatePath, AStarMode.WeightedPath);
        }
    }
}
