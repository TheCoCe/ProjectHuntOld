﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    public class BehaviourFlee : BehaviourState
    {
        GridCubeCoord destination;
        int fleeRadius = 5;

        protected override void CreatePath(List<GridTile> path, bool success)
        {
            if (success && path != null && path.Count > 0)
            {
                for (int i = 0; i < path.Count; i++)
                {
                    if (myGroup.AttemptMove(path[i].Coord, path[i].MovementCost))
                        Path.Enqueue(path[i]);
                    else
                    {
                        Move(path, i + 1);
                        return;
                    }

                    //Arrival Check
                    if (path[i].Coord == (GridCubeCoord)destination)
                    { //Update player in range. If player too close, keep fleeing, else go back to nest
                        if (PlayerInRange())
                        {
                            Move(path, i + 1);
                            return;
                        }
                        else
                        {
                            Move(path, i + 1);
                            ChangeBehaviour(AIBehaviourType.GoToNest);
                            return;
                        }
                    }
                }
                
                
            }
            if (PlayerInRange())
            {
                GridTile gridTile;

                Vector3 playerPosition = (GridCoord)MapData.Instance.FindClosestPlayerCoord(myGroup.Position);
                Vector3 myPosition = (GridCoord)myGroup.Position;
                Vector3 direction = (myPosition - playerPosition).normalized * GridCubeCoord.Size;
                int directionIndex = (int)GridCubeCoord.GetNeighbourDirection(myGroup.Position, myGroup.Position + (GridCoord)direction);
                List<GridCubeCoord> ring = GridCubeCoord.GetTilesInRing(myGroup.Position, fleeRadius);

                int index = (directionIndex * fleeRadius + 2 * fleeRadius) % ring.Count;
                do
                {
                    destination = ring[index];
                    index = (index + 1) % ring.Count;
                } while (!MapData.Instance.GetTileAtCoord(destination, out gridTile) || !gridTile.IsWalkable);

                Path.Enqueue(MapData.Instance.GetTileAtCoord(myGroup.Position));
                PathRequestManager.RequestPath(myGroup.Position, destination, CreatePath, AStarMode.ShortestPath);
            }
            else
            {
                ChangeBehaviour(AIBehaviourType.GoToNest);
            }
        }
    }
}
