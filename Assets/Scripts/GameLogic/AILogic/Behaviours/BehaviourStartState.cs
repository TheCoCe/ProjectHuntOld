﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    public class BehaviourStartState : BehaviourState
    {
        private AIBehaviourType nextState;

        public BehaviourStartState(AIGroup group, AIBehaviourType nextType = AIBehaviourType.RandomRoaming)
        {
            //Stuff that needs to be inistialized for the States
            Path = new Queue<GridTile>();
            myGroup = group;
            this.nextState = nextType;
        }

        public override void HandleTurn()
        {
            pathCreationInProgress = true;
            ChangeBehaviour(nextState);
        }

        protected override void CreatePath(List<GridTile> path, bool success)
        {
        }
    }
}
