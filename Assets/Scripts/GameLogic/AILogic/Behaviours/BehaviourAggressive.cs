﻿using System;
using System.Collections.Generic;

namespace ProjectHunt
{
    class BehaviourAggressive : BehaviourState
    {
        GridCoord destination;
        int outOfReachDelay = 5;
        private static int maxRoamDistance = 5;
        private static int minVillageDistance = 5;

        protected override void CreatePath(List<GridTile> path, bool success)
        {
            if (success && path != null && path.Count > 0)
            {
                for (int i = 0; i < path.Count; i++)
                {
                    //Check if we have AP left. If not move along the path that was created until now.
                    if (myGroup.AttemptMove(path[i].Coord, path[i].MovementCost))
                        Path.Enqueue(path[i]);
                    else
                    {
                        Move();
                        return;
                    }

                    //Arrival Check
                    if (path[i].Coord == (GridCubeCoord)destination)
                    {
                        PlayerInRange();
                        Move();
                        ChangeBehaviour(AIBehaviourType.RandomRoaming);
                        return;
                    }

                    //Player Check
                    if (!PlayerInRange())
                    { //Aggressive State but Player group out of reach!
                        outOfReachDelay -= 1;
                        if (outOfReachDelay <= 0)
                        { //Rounds the monster can be in aggressive State without finding a player <= 0
                            ChangeBehaviour(AIBehaviourType.RandomRoaming);
                            return;
                        }
                    }
                }
            }

            destination = GetPlayerInRangePosition();
            if ((GridCubeCoord)destination == GridCubeCoord.invalid)
            {
                //outOfReachDelay -= 1;
                //if (outOfReachDelay <= 0)
                //{
                //    ChangeBehaviour(AIBehaviourType.RandomRoaming);
                //    return;
                //}
                //else
                { //Aggressive but no Player in Range to target
                    GridTile gridTile;
                    do
                    {
                        destination.x = ((GridCoord)myGroup.Position).x + UnityEngine.Random.Range(-maxRoamDistance, maxRoamDistance);
                        destination.y = ((GridCoord)myGroup.Position).y + UnityEngine.Random.Range(-maxRoamDistance, maxRoamDistance);
                    } while (!(MapData.Instance.GetTileAtCoord(destination, out gridTile)) || gridTile.IsWalkable == false);

                    Path.Enqueue(MapData.Instance.GetTileAtCoord(myGroup.Position));
                    PathRequestManager.RequestPath(myGroup.Position, destination, CreatePath, AStarMode.ShortestPath);
                    return;
                }
            }
            else
            {
                outOfReachDelay = 5;
            }

            Path.Enqueue(MapData.Instance.GetTileAtCoord(myGroup.Position));
            PathRequestManager.RequestPath(myGroup.Position, destination, CreatePath, AStarMode.ShortestPath);
        }
    }
}
