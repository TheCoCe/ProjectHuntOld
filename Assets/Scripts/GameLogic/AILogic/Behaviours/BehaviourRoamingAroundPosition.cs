﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    public class BehaviourRoamingAroundPosition : BehaviourState
    {
        private GridCubeCoord center = GridCubeCoord.invalid;
        private GridCoord destination;
        private static int radius = 2;
        private static int maxIterations = 5;
        private static int minIterations = 2;
        private int iteration = 0;

        /*
        public void CreatePath(AIGroup group, Action<List<GridTile>, bool> callback)
        {
            if (center == (GridCoord)GridCubeCoord.invalid)
            {
                center = group.Position;
            }

            GridTile gridTile;
            do
            {
                destination.x = center.x + UnityEngine.Random.Range(-radius, radius);
                destination.y = center.y + UnityEngine.Random.Range(-radius, radius);
            } while (!(MapData.Instance.GetTileAtCoord(destination, out gridTile)) || gridTile.IsWalkable == false);

            PathRequestManager.RequestPath(group.Position, destination, callback, AStarMode.WeightedPath);
            iteration++;
        }

        public BehaviourState ChangeState()
        {
            if(UnityEngine.Random.Range(minIterations, maxIterations) <= iteration)
            {
                Debug.Log("<color=green>Switching Behaviour to: " + typeof(RandomRoaming) + "</color>");
                return new RandomRoaming();
            }
            return this;
        }
        */
        protected override void CreatePath(List<GridTile> path, bool success)
        {
            if (success && path != null)
            {
                for (int i = 0; i < path.Count; i++)
                {
                    //Check if we have AP left. If not move along the path that was created until now.
                    if (myGroup.AttemptMove(path[i].Coord, path[i].MovementCost))
                        Path.Enqueue(path[i]);
                    else
                    {
                        Move();
                        return;
                    }

                    //Arrival Check
                    if (path[i].Coord == (GridCubeCoord)destination)
                    {
                        PlayerInRange();
                        if(myGroup.CurrentAP <= 0 && UnityEngine.Random.Range(minIterations, maxIterations) <= iteration)
                        {
                            //Break and move to a new random tile in range
                            break;
                        }
                        Move();
                        return;
                    }

                    //Player Check
                    if (PlayerInRange())
                    {
                        PathRequestManager.RequestPath(path[i].Coord, destination, CreatePath, AStarMode.WeightedPath);
                        return;
                    }
                }
            }

            GridTile gridTile;
            do
            {
                destination.x = ((GridCoord)center).x + UnityEngine.Random.Range(-radius, radius);
                destination.y = ((GridCoord)center).y + UnityEngine.Random.Range(-radius, radius);
            } while (!(MapData.Instance.GetTileAtCoord(destination, out gridTile)) || gridTile.IsWalkable == false);

            Path.Enqueue(MapData.Instance.GetTileAtCoord(myGroup.Position));
            PathRequestManager.RequestPath(myGroup.Position, destination, CreatePath, AStarMode.WeightedPath);
            iteration++;
        }
    }
}
