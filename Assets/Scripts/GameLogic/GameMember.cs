﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    public class GameMember : ScriptableObject
    {
        #region Data
        public string Name { get; private set; }
        protected Dictionary<uint, Group> groups;
        public bool Controllable { get; private set; }
        public /*static*/ uint generatedGroupID = 0;
        #endregion

        #region Properties
        public uint MemberID
        {
            get;
            private set;
        }
        public /*static*/ uint GenerateGroupID
        {
            get
            {
                return generatedGroupID++;
            }
        }
        #endregion

        #region Events
        public delegate void GroupCreated(Group group);
        public static event GroupCreated OnGroupCreated;
        public delegate void GroupRemoved(Group group, Group nextGroup);
        public static event GroupRemoved OnGroupRemoved;
        
        protected virtual void Awake()
        {
            groups = new Dictionary<uint, Group>();
        }
        #endregion

        #region Methods
        public void Init(uint memberID, string name, bool controllable)
        {
            this.MemberID = memberID;
            Name = name + MemberID;
            Controllable = controllable;
        }

        public virtual Group CreateGroup(GridCubeCoord coord, Entity[] members, int currentAp = -1, int maxAP = -1)
        {
            Group newGroup = ScriptableObject.CreateInstance<Group>();
            newGroup.Init(GenerateGroupID, MemberID, members, coord, currentAp, maxAP);

            groups.Add(newGroup.GroupID, newGroup);
            Debug.Log("Generated Group with ID: " + newGroup.GroupID);
            OnGroupCreated?.Invoke(newGroup);
            return newGroup;
        }

        public virtual void RemoveGroup(Group group)
        {
            MapData.Instance.DecreaseVisibility(group.Position, group.ViewDistance);
            groups.Remove(group.GroupID);
        }

        public bool HasGroup(Group group)
        {
            if (groups.ContainsValue(group))
                return true;
            return false;
        }

        public bool HasGroup(uint groupID)
        {
            if (groups.ContainsKey(groupID))
                return true;
            return false;
        }

        public Group GetFirstGroup()
        {
            Group group;
            uint min = 0;
            bool first = true;
            foreach (uint groupID in groups.Keys)
            {
                if (first)
                {
                    min = groupID;
                    first = false;
                }
                else if (groupID < min)
                    min = groupID;
            }
            if (groups.TryGetValue(min, out group))
                return group;
            return null;
        }

        public Dictionary<uint, Group> GetGroups()
        {
            return groups;
        }

        public Group GetGroupByID(uint groupID)
        {
            Group group;
            if (groups.TryGetValue(groupID, out group))
                return group;
            return null;
        }

        public GridCubeCoord[] GetGroupPositions()
        {
            GridCubeCoord[] groupCoords = new GridCubeCoord[groups.Count];
            int index = 0;
            foreach (var item in groups.Values)
            {
                groupCoords[index] = item.Position;
                index++;
            }
            return groupCoords;
        }

        public int[] GetGroupsAP()
        {
            int[] ap = new int[groups.Count];
            int index = 0;
            foreach (var item in groups.Values)
            {
                ap[index] = item.CurrentAP;
                index++;
            }
            return ap;
        }

        public bool MoveEntityFromTo(uint fromGroupId, uint toGroupId, Entity[] membersToSwitch)
        {
            if(groups.ContainsKey(fromGroupId) && groups.ContainsKey(toGroupId))
            {
                for(int i = 0; i < membersToSwitch.Length; i++)
                {
                    groups[fromGroupId].RemoveMember(membersToSwitch[i]);
                    groups[toGroupId].AddMember(membersToSwitch[i]);
                }
                if (groups[fromGroupId].Size <= 0)
                {
                    OnGroupRemoved?.Invoke(groups[fromGroupId], groups[toGroupId]);
                    RemoveGroup(groups[fromGroupId]);
                }

                return true;
            }
            return false;
        }

        public bool SplitGroup(Group group, Entity[] membersToSwitch)
        {
            if (groups.ContainsKey(group.GroupID))
            {
                for (int i = 0; i < membersToSwitch.Length; i++)
                    group.RemoveMember(membersToSwitch[i]);

                Group newGroup = CreateGroup(group.Position, membersToSwitch, group.CurrentAP);

                if (group.Size <= 0)
                {
                    OnGroupRemoved?.Invoke(group, newGroup);
                    RemoveGroup(group);
                }

                //Group newGroup = ScriptableObject.CreateInstance<Group>();
                //newGroup.Init(GenerateGroupID, MemberID, membersToSwitch, group.Position);
                //OnGroupCreated?.Invoke(newGroup);
                return true;
            }
            return false;
        }

        public bool SplitGroup(uint groupID, Entity[] membersToSwitch)
        {
            if (groups.ContainsKey(groupID))
            {
                return SplitGroup(groups[groupID], membersToSwitch);
            }
            return false;
        }

        public bool SplitGroupHalf(Group group)
        {
            List<Entity> membersToSwitch = new List<Entity>();
            if (group.GetMembers().Count >= 2)
            {
                int counter = 0;
                var items = group.GetMembers();
                foreach (var item in items)
                {
                    membersToSwitch.Add(item);
                    counter++;
                    if (counter >= group.GetMembers().Count / 2)
                    {
                        break;
                    }
                }

                return SplitGroup(group, membersToSwitch.ToArray());

            }

            return false;
        }

        public virtual void NewTurn()
        {
            foreach (var group in groups.Values)
            {
                group.ResetAP();
            }
        }

        public static void CallOnGroupCreated(Group group)
        {
            OnGroupCreated?.Invoke(group);
        }
        #endregion
    }
}