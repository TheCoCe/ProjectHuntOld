﻿using UnityEngine;
using UnityEngine.UI;

namespace ProjectHunt
{
    [CreateAssetMenu(menuName = "Project Hunt/Logic Elements/Entity")]
    public class Entity : ScriptableObject
    {
        #region Data
        public string entityName = "Entity Name";
        public Sprite portrait;
        [SerializeField]
        private int maxHp;
        [SerializeField]
        private int hp;
        [SerializeField]
        private PassiveAbility passiveAbility;
        [SerializeField]
        private ActiveAbility activeAbility;
        #endregion

        #region Properties
        public int MaxHP
        {
            get
            {
                return maxHp;
            }
            set
            {
                maxHp = value;
            }
        }

        public int HP
        {
            get
            {
                return hp;
            }
            set
            {
                hp = value;
            }
        }
        #endregion

        #region Events

        #endregion

        #region Methods
        public void ModifyByPassive(Group group)
        {
            if (passiveAbility != null)
                passiveAbility.ModifyByPassive(group);
        }

        public void RevertModification(Group group)
        {
            if (passiveAbility != null)
                passiveAbility.RevertModification(group);
        }

        public void TriggerAbility(GridCubeCoord position)
        {
            if (activeAbility != null)
                activeAbility.TriggerAbility(position);
        }
        #endregion
    }
}