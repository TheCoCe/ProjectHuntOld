﻿using UnityEngine;
using ProjectHunt;

public class SetupManager : MonoBehaviour
{
    #region Data
    public static SetupManager Instance
    {
        get;
        private set;
    }

    public static bool IsSingleplayer
    {
        get;
        set;
    }

    public SetupData setupData;
    #endregion

    #region Events
    public delegate void GameSetup();
    public event GameSetup OnGameSetup;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if(Instance != null)
            Destroy(gameObject);
    }

    private void Start()
    {
        IsSingleplayer = true;
        NewGame();
        OnGameSetup?.Invoke();
    }

    #endregion

    #region Methods
    private void NewGame()
    {
        if (setupData != null)
        {
            Debug.Log("Setup Manager...");
            TurnManager.Instance.ClearGameMembers();
            for (int i = 0; i < setupData.gameMembers.Count; i++)
            {
                GameMember newGameMember = TurnManager.Instance.NewGameMember(!setupData.gameMembers[i].isAI);
                for (int j = 0; j < setupData.gameMembers[i].groups.Count; j++)
                {
                    Entity[] entities = new Entity[setupData.gameMembers[i].groups[j].entities.Count];
                    for (int k = 0; k < setupData.gameMembers[i].groups[j].entities.Count; k++)
                    {
                        entities[k] = setupData.gameMembers[i].groups[j].entities[k].entity;
                    }

                    if (setupData.gameMembers[i].isAI)
                    {
                        (newGameMember as AIGameMember).CreateGroup(
                            setupData.gameMembers[i].groups[j].gridCoordSpawnPosition,
                            entities,
                            setupData.gameMembers[i].groups[j].currentAP,
                            setupData.gameMembers[i].groups[j].maxAP,
                            setupData.gameMembers[i].groups[j].startingBehaviour);
                    }
                    else
                    {
                        newGameMember.CreateGroup(
                            setupData.gameMembers[i].groups[j].gridCoordSpawnPosition,
                            entities,
                            setupData.gameMembers[i].groups[j].currentAP,
                            setupData.gameMembers[i].groups[j].maxAP);
                    }
                }
            }
        }
        else
        {
            Debug.LogWarning("SetupManager fallback! Make sure the SetupData is configured for this Scene!");
            TurnManager.Instance.ClearGameMembers();
            GameMember newGameMember = TurnManager.Instance.NewGameMember(true);
            Entity[] hunters = Resources.LoadAll<Entity>("Hunter");
            newGameMember.CreateGroup(new GridCubeCoord(0, 0, 0), hunters);

            newGameMember = TurnManager.Instance.NewGameMember(false);
            Entity[] monsters = Resources.LoadAll<Entity>("Monster");
            (newGameMember as AIGameMember)?.CreateGroup(new GridCubeCoord(0, 0, 0), monsters);
        }
    }
    #endregion
}
