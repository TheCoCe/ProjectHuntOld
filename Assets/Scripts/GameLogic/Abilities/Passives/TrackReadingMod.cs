﻿using UnityEngine;

namespace ProjectHunt
{
    [CreateAssetMenu(menuName = "Project Hunt/Logic Elements/Passive Abilities/Track Reading Modification")]
    public class TrackReadingMod : PassiveAbility
    {
        [SerializeField]
        private float trackReadingMod = 0.1f;

        public override void ModifyByPassive(Group group)
        {
            group.ModifyTrackReading(trackReadingMod);
        }

        public override void RevertModification(Group group)
        {
            group.ModifyTrackReading(-trackReadingMod);
        }
    }
}