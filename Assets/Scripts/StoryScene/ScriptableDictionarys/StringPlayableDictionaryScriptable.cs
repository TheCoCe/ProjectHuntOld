﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;


[CreateAssetMenu(fileName = "PlayableDictionary", menuName = "ProjectHunt/PlayableDictionary")]
public class StringPlayableDictionaryScriptable : ScriptableObject
{
    [SerializeField]
    private StringPlayableDictionary playables;

    public StringPlayableDictionary Playables
    {
        get
        {
            return playables;
        }
    }

}
