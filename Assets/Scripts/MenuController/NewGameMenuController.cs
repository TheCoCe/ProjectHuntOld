﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NewGameMenuController : MonoBehaviour
{
    #region Data
    private bool singleOrCoop;
    [SerializeField]
    private Text singleOrCoopText;
    #endregion

    #region Events
    private void Start()
    {
        singleOrCoop = false;
    }
    #endregion

    public void NumberOfPlayers()
    {
        singleOrCoop = !singleOrCoop;
        if(singleOrCoopText != null)
        {
            if (singleOrCoop)
            {
                singleOrCoopText.text = "Singleplayer";
            }
            else
            {
                singleOrCoopText.text = "Coop";
            }
        }
    }

    public void SelectionOfQuests()
    {
        Debug.Log("Do something!");
    }

    public void StartGame()
    {
        //Load or start a new game
    }
}