﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    public enum DaytimeState { Dawn = 0, Day = 1, Dusk = 2, Night = 3 };

    public class DayNightCycle : MonoBehaviour {

        #region Data
        public Light sun;
        public Gradient sunColorGradient;
        public float transitionSpeed = 0.1f;
        public DaytimeState startDaytime;
        [Header("0 = Dawn, 1 = Day, 2 = Dusk, 3 = Night")]
        public int[] daytimeStateRounds = { 1, 4, 1, 2};
        public DaytimeState curDaytime;

        public bool cycleDone = false;

        public static DaytimeState CurrentDaytime
        {
            get;
            private set;
        }
        public static int Days;

        public static DayNightCycle Instance
        {
            get;
            private set;
        }

        [SerializeField]
        private float currentTimeOfDay = 0;
        private float sunInitialIntensity;
        private int currentState = 1;
        #endregion

        #region Events
        public delegate void DaytimeChanged(DaytimeState currentState);
        public static DaytimeChanged OnDaytimeChanged;

        private void Awake()
        {
            if(daytimeStateRounds == null)
            {
                daytimeStateRounds = new int[4];
                daytimeStateRounds[0] = 1;
                daytimeStateRounds[1] = 4;
                daytimeStateRounds[2] = 1;
                daytimeStateRounds[3] = 2;
            }
            Days = 1;
            sun = GetComponent<Light>();
            Instance = this;
        }

        private void OnEnable()
        {
            TurnManager.OnRoundEnd += AdvanceCycleByOne;
        }
    
        private void OnDisable()
        {
            TurnManager.OnRoundEnd -= AdvanceCycleByOne;
        }

        void Start()
        {
            sunInitialIntensity = sun.intensity;
            CurrentDaytime = curDaytime = startDaytime;

            currentTimeOfDay = CalculateCurrentTimeOfDay();
            sun.transform.localRotation = Quaternion.Euler((CalculateCurrentTimeOfDay() * 360f) - 90, 170, 0);
        }
        /*
        void Update()
        {
            sun.transform.localRotation = Quaternion.Euler((currentTimeOfDay * 360f) - 90, 170, 0);

            float intensityMultiplier = 1;
            if (currentTimeOfDay <= 0.23f || currentTimeOfDay >= 0.75f)
            {
                intensityMultiplier = 0;
            }
            else if (currentTimeOfDay <= 0.25f)
            {
                intensityMultiplier = Mathf.Clamp01((currentTimeOfDay - 0.23f) * (1 / 0.02f));
            }
            else if (currentTimeOfDay >= 0.73f)
            {
                intensityMultiplier = Mathf.Clamp01(1 - ((currentTimeOfDay - 0.73f) * (1 / 0.02f)));
            }

            sun.intensity = sunInitialIntensity * intensityMultiplier;
        }
        */
        #endregion

        #region Methods
        public void AdvanceCycleByOne()
        {
            currentState += 1;
            if(currentState > daytimeStateRounds[(int)CurrentDaytime])
            {
                currentState = 1;
                CurrentDaytime = (DaytimeState)(((int)CurrentDaytime + 1) % 4);
                if(CurrentDaytime == DaytimeState.Dawn)
                    Days++;
                curDaytime = CurrentDaytime;
                OnDaytimeChanged?.Invoke(CurrentDaytime);
            }

            StartCoroutine(Cycle(CalculateCurrentTimeOfDay()));

        }

        public void AdvanceDaytimeTo(DaytimeState newState)
        {
            cycleDone = false;
            currentState = 1;
            if (newState <= CurrentDaytime)
                Days++;
            CurrentDaytime = newState;
            OnDaytimeChanged?.Invoke(CurrentDaytime);
            StartCoroutine(Cycle(CalculateCurrentTimeOfDay()));
        }

        private IEnumerator Cycle(float targetTimeOfDay)
        {
            float maxCycleAmount, currentCycleAmount = 0;
            if(currentTimeOfDay < targetTimeOfDay)
                maxCycleAmount = targetTimeOfDay - currentTimeOfDay;
            else if (currentTimeOfDay > targetTimeOfDay)
                maxCycleAmount = 1 - currentTimeOfDay + targetTimeOfDay;
            else
                maxCycleAmount = 1;

            while (currentCycleAmount < maxCycleAmount)
            {
                currentCycleAmount += Time.deltaTime * transitionSpeed;

                currentTimeOfDay += Time.deltaTime * transitionSpeed;
                if (currentTimeOfDay > 1f)
                    currentTimeOfDay = 0f;

                sun.transform.localRotation = Quaternion.Euler((currentTimeOfDay * 360f) - 90, 170, 0);
                sun.color = sunColorGradient.Evaluate(currentTimeOfDay);

                float intensityMultiplier = 1;
                if (currentTimeOfDay <= 0.23f || currentTimeOfDay >= 0.75f)
                {
                    intensityMultiplier = 0;
                }
                else if (currentTimeOfDay <= 0.25f)
                {
                    intensityMultiplier = Mathf.Clamp01((currentTimeOfDay - 0.23f) * (1 / 0.02f));
                }
                else if (currentTimeOfDay >= 0.73f)
                {
                    intensityMultiplier = Mathf.Clamp01(1 - ((currentTimeOfDay - 0.73f) * (1 / 0.02f)));
                }

                sun.intensity = sunInitialIntensity * intensityMultiplier;
                yield return null;
            }
            cycleDone = true;
        }

        private float CalculateCurrentTimeOfDay()
        {
            float targetTimeOfDay = 0f;
            switch (CurrentDaytime)
            { //TODO: Adjust Values
                //0.28 Dawn
                //0.30 Day
                //0.72 Dusk
                //0.75 Night
                case DaytimeState.Dawn:
                    //Ab 0.22 bis 0.26
                    targetTimeOfDay = ((0.30f - 0.28f) / (daytimeStateRounds[(int)DaytimeState.Dawn] + 1)) * currentState + 0.28f;
                    break;
                case DaytimeState.Day:
                    //0.26 bis 0.74
                    targetTimeOfDay = ((0.71f - 0.30f) / (daytimeStateRounds[(int)DaytimeState.Day] + 1)) * currentState + 0.30f;
                    break;
                case DaytimeState.Dusk:
                    //0.74 bis 0.77
                    targetTimeOfDay = ((0.75f - 0.71f) / (daytimeStateRounds[(int)DaytimeState.Dusk] + 1)) * currentState + 0.71f;
                    break;
                case DaytimeState.Night:
                    //0.77 bis 0.22
                    targetTimeOfDay = (((1 - 0.75f + 0.28f) / (daytimeStateRounds[(int)DaytimeState.Night] + 1)) * currentState + 0.75f) % 1f;
                    break;
                default:
                    break;
            }
            return targetTimeOfDay;
        }
        #endregion


    }
}
