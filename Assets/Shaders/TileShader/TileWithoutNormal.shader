﻿Shader "Tiles/TileWithoutNormal" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_BackgroundColor("Background Color", Color) = (0,0,0)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Specular("Specular", Color) = (0.2, 0.2, 0.2)
	}
	SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf StandardSpecular fullforwardshadows vertex:vert

		#pragma target 3.0

		#include "TileData.cginc"

		sampler2D _MainTex;
		sampler2D _BumpMap;

		struct Input {
			float2 uv_MainTex;
			float2 visibility;
		};

		half _Glossiness;
		fixed4 _Color;
		fixed3 _Specular;
		half3 _BackgroundColor;

		void vert(inout appdata_full v, out Input data) {
			UNITY_INITIALIZE_OUTPUT(Input, data);

			float4 cellData = GetCellData(v.texcoord3);
			data.visibility.x = cellData.x;
			data.visibility.y = cellData.y;
			data.visibility.x = lerp(0.25, 1, data.visibility.x);
		}

		void surf(Input IN, inout SurfaceOutputStandardSpecular o) {

			float explored = IN.visibility.y;

			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb * IN.visibility.x * explored;

			o.Specular = _Specular * explored;
			o.Smoothness = _Glossiness * IN.visibility.y;
			o.Occlusion = explored;
			o.Alpha = c.a;
			o.Emission = _BackgroundColor * (1 - explored);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
