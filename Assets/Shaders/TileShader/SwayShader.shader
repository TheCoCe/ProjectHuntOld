﻿﻿Shader "Tiles/SwayInWind" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_BackgroundColor("Background Color", Color) = (0,0,0)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_BumpMap ("Normal map", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Specular("Specular", Color) = (0.2, 0.2, 0.2)
		_Cutoff("Alpha cutoff", Float) = .5


// Global wind direction / speed
		
		_WindSpeed("Wind speed", Float) = 1
		_VertSwaySpeed("Vert Sway speed", Float) = 1
		_VertHookSpeed("Vert Hook speed", Float) = 1
		_VertHookMag("Vert Hook mag", Float) = 1
		_RngByPhysicalDistance("Rng by distance", Float) = 1
	}
	SubShader {
		Tags { "RenderType"="Opaque"  }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf StandardSpecular fullforwardshadows alphatest:_Cutoff vertex:vert addshadow
		#pragma target 3.0

		#include "TileData.cginc"

		sampler2D _MainTex;
		sampler2D _BumpMap;

		struct Input {
			float2 uv_MainTex;
			float2 uv_BumpMap;
			float2 visibility;
		};
		

		half _Glossiness;
		fixed4 _Color;
		fixed3 _Specular;
		half3 _BackgroundColor;

		float _WindSpeed;
		float _RngByPhysicalDistance;
		float _VertSwaySpeed;
		float _VertHookSpeed;
		float _VertHookMag;

		void vert(inout appdata_full v, out Input data)
		{
			UNITY_INITIALIZE_OUTPUT(Input, data);

			// Get CellShaderData
			float4 cellData = GetCellData(v.texcoord3);
			data.visibility.x = cellData.x;
			data.visibility.y = cellData.y;
			data.visibility.x = lerp(0.25, 1, data.visibility.x);

			//Sway Shader
			float rngPosition = sin(length(mul(unity_ObjectToWorld, float4(0,0,0,1)).xz) * _RngByPhysicalDistance);
			float rngTime = sin(_Time.x * _VertSwaySpeed);

			float rngTime2 = sin(_Time.y * _VertHookSpeed);

			float temp = v.vertex.z * rngPosition * rngTime * _WindSpeed;
			v.vertex.x += temp * sin(rngTime2 + v.vertex.z * _VertHookMag) * clamp(v.vertex.y, 0, 1);
		}

		void surf (Input IN, inout SurfaceOutputStandardSpecular o) {

			float explored = IN.visibility.y;

			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb * IN.visibility.x * explored;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));

			o.Specular = _Specular * explored;
			o.Smoothness = _Glossiness;
			o.Occlusion = explored;
			o.Alpha = c.a;
			o.Emission = _BackgroundColor * (1 - explored);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
